/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.BehavioralsignalsUapiClient);
  }
}(this, function(expect, BehavioralsignalsUapiClient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new BehavioralsignalsUapiClient.BookmarkModel();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('BookmarkModel', function() {
    it('should create an instance of BookmarkModel', function() {
      // uncomment below and update the code to test BookmarkModel
      //var instance = new BehavioralsignalsUapiClient.BookmarkModel();
      //expect(instance).to.be.a(BehavioralsignalsUapiClient.BookmarkModel);
    });

    it('should have the property bookmarkcategoryId (base name: "bookmarkcategory_id")', function() {
      // uncomment below and update the code to test the property bookmarkcategoryId
      //var instance = new BehavioralsignalsUapiClient.BookmarkModel();
      //expect(instance).to.be();
    });

    it('should have the property callId (base name: "call_id")', function() {
      // uncomment below and update the code to test the property callId
      //var instance = new BehavioralsignalsUapiClient.BookmarkModel();
      //expect(instance).to.be();
    });

  });

}));
