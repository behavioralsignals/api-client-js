/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.BehavioralsignalsUapiClient);
  }
}(this, function(expect, BehavioralsignalsUapiClient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('ResponseHighlightNote', function() {
    it('should create an instance of ResponseHighlightNote', function() {
      // uncomment below and update the code to test ResponseHighlightNote
      //var instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
      //expect(instance).to.be.a(BehavioralsignalsUapiClient.ResponseHighlightNote);
    });

    it('should have the property userFullname (base name: "user_fullname")', function() {
      // uncomment below and update the code to test the property userFullname
      //var instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
      //expect(instance).to.be();
    });

    it('should have the property userPic (base name: "user_pic")', function() {
      // uncomment below and update the code to test the property userPic
      //var instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
      //expect(instance).to.be();
    });

    it('should have the property datetime (base name: "datetime")', function() {
      // uncomment below and update the code to test the property datetime
      //var instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
      //expect(instance).to.be();
    });

    it('should have the property comment (base name: "comment")', function() {
      // uncomment below and update the code to test the property comment
      //var instance = new BehavioralsignalsUapiClient.ResponseHighlightNote();
      //expect(instance).to.be();
    });

  });

}));
