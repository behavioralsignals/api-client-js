/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.BehavioralsignalsUapiClient);
  }
}(this, function(expect, BehavioralsignalsUapiClient) {
  'use strict';

  var instance;

  beforeEach(function() {
    instance = new BehavioralsignalsUapiClient.CampaignsApi();
  });

  var getProperty = function(object, getter, property) {
    // Use getter method if present; otherwise, get the property directly.
    if (typeof object[getter] === 'function')
      return object[getter]();
    else
      return object[property];
  }

  var setProperty = function(object, setter, property, value) {
    // Use setter method if present; otherwise, set the property directly.
    if (typeof object[setter] === 'function')
      object[setter](value);
    else
      object[property] = value;
  }

  describe('CampaignsApi', function() {
    describe('createCampaign', function() {
      it('should call createCampaign successfully', function(done) {
        //uncomment below and update the code to test createCampaign
        //instance.createCampaign(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('deleteCampaign', function() {
      it('should call deleteCampaign successfully', function(done) {
        //uncomment below and update the code to test deleteCampaign
        //instance.deleteCampaign(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getAggregatedCampaign', function() {
      it('should call getAggregatedCampaign successfully', function(done) {
        //uncomment below and update the code to test getAggregatedCampaign
        //instance.getAggregatedCampaign(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getAggregatedCampaigns', function() {
      it('should call getAggregatedCampaigns successfully', function(done) {
        //uncomment below and update the code to test getAggregatedCampaigns
        //instance.getAggregatedCampaigns(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getCampaign', function() {
      it('should call getCampaign successfully', function(done) {
        //uncomment below and update the code to test getCampaign
        //instance.getCampaign(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('getCampaigns', function() {
      it('should call getCampaigns successfully', function(done) {
        //uncomment below and update the code to test getCampaigns
        //instance.getCampaigns(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
    describe('updateCampaign', function() {
      it('should call updateCampaign successfully', function(done) {
        //uncomment below and update the code to test updateCampaign
        //instance.updateCampaign(function(error) {
        //  if (error) throw error;
        //expect().to.be();
        //});
        done();
      });
    });
  });

}));
