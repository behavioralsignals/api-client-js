# BehavioralsignalsUapiClient.ResponseHighlightsAgent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**compliance** | **Boolean** |  | [optional] 
**picUrl** | **String** |  | [optional] 


