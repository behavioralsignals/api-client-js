# BehavioralsignalsUapiClient.CalculationModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**message** | **String** |  | [optional] 
**status** | **Number** |  | [optional] 
**estimatedTime** | **Number** | time in seconds | [optional] 


