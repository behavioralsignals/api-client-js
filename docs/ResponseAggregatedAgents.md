# BehavioralsignalsUapiClient.ResponseAggregatedAgents

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ResponseAggregatedAgent]**](ResponseAggregatedAgent.md) |  | [optional] 


