# BehavioralsignalsUapiClient.ResponseAggregations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**impact** | [**ScoredIndicatorType**](ScoredIndicatorType.md) |  | [optional] 
**highlights** | [**[Call]**](Call.md) | A list of call highlights | [optional] 
**agentTime** | **Number** | Agent talk time in seconds | [optional] 
**customerTime** | **Number** | Customer talk time in seconds | [optional] 
**overlapTime** | **Number** | Overlap time in seconds | [optional] 
**behaviors** | [**[CallDetailsBehaviors]**](CallDetailsBehaviors.md) |  | [optional] 
**trends** | [**CallDetailsTrends**](CallDetailsTrends.md) |  | [optional] 


