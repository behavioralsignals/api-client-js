# BehavioralsignalsUapiClient.TenantApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTenant**](TenantApi.md#createTenant) | **POST** /tenant/ | Creates a new tenant on all services
[**revokeTenant**](TenantApi.md#revokeTenant) | **DELETE** /tenant/ | Revokes a tenant from all services


<a name="createTenant"></a>
# **createTenant**
> ResponseTenantCreation createTenant(tenantCreationRequest)

Creates a new tenant on all services

Creates a new tenant

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.TenantApi();

var tenantCreationRequest = new BehavioralsignalsUapiClient.CreateTenant(); // CreateTenant | Tenant creation object

apiInstance.createTenant(tenantCreationRequest).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tenantCreationRequest** | [**CreateTenant**](CreateTenant.md)| Tenant creation object | 

### Return type

[**ResponseTenantCreation**](ResponseTenantCreation.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="revokeTenant"></a>
# **revokeTenant**
> SuccessModel revokeTenant(opts)

Revokes a tenant from all services

Revoke tenant

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.TenantApi();

var opts = { 
  'name': "name_example" // String | Name of tenant to be deleted
};
apiInstance.revokeTenant(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**| Name of tenant to be deleted | [optional] 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

