# BehavioralsignalsUapiClient.Campaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | Unique identifier for the campaign | [optional] 
**campaignId** | **String** | String identifier of campaign | [optional] 
**title** | **String** | The title of the campaign | 
**startDate** | **Date** | Starting date of the campaign range | [optional] 
**endDate** | **Date** | Ending date of the campaign range | [optional] 
**agents** | **[String]** | List of agents whose calls should be assigned to the campaign | 


