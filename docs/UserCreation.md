# BehavioralsignalsUapiClient.UserCreation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**username** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**email** | **String** |  | [optional] 
**details** | [**UserCreationDetails**](UserCreationDetails.md) |  | [optional] 


