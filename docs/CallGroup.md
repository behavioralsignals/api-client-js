# BehavioralsignalsUapiClient.CallGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | **[String]** |  | 
**id** | **Number** | The id of the campaign | [optional] 
**campaignId** | **String** |  | [optional] 
**startDate** | **Date** |  | [optional] 
**endDate** | **Date** |  | [optional] 


