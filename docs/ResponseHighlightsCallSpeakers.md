# BehavioralsignalsUapiClient.ResponseHighlightsCallSpeakers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | [**ResponseHighlightsAgent**](ResponseHighlightsAgent.md) |  | 
**customer** | [**ResponseHighlightsCustomer**](ResponseHighlightsCustomer.md) |  | 


