# BehavioralsignalsUapiClient.ResponseHighlightsCall

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**speakers** | [**ResponseHighlightsCallSpeakers**](ResponseHighlightsCallSpeakers.md) |  | 
**datetime** | **Date** |  | 
**campaignTitles** | **[String]** |  | [optional] 
**playbackUrl** | **String** |  | 
**calldirection** | **Number** |  | [optional] 
**duration** | **String** |  | 
**frames** | [**[ResponseHighlightsFrame]**](ResponseHighlightsFrame.md) |  | 


