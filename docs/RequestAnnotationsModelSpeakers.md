# BehavioralsignalsUapiClient.RequestAnnotationsModelSpeakers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | [**AnnotationSpeaker**](AnnotationSpeaker.md) |  | 
**customer** | [**AnnotationSpeaker**](AnnotationSpeaker.md) |  | 


