# BehavioralsignalsUapiClient.AnnotationsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**removeCallLock**](AnnotationsApi.md#removeCallLock) | **DELETE** /calls/{id}/lock/ | Removes the annotation lock of a call
[**updateCallAnnotation**](AnnotationsApi.md#updateCallAnnotation) | **PUT** /calls/{call_id}/annotations/ | Annotate highlights for a call
[**updateCallLock**](AnnotationsApi.md#updateCallLock) | **PUT** /calls/{id}/lock/ | Locks or updates the annotation lock of a call


<a name="removeCallLock"></a>
# **removeCallLock**
> SuccessModel removeCallLock(id)

Removes the annotation lock of a call

Removes the annotation lock of a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AnnotationsApi();

var id = 56; // Number | The call id

apiInstance.removeCallLock(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The call id | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCallAnnotation"></a>
# **updateCallAnnotation**
> SuccessModel updateCallAnnotation(callId, opts)

Annotate highlights for a call

Annotate highlights for a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AnnotationsApi();

var callId = 56; // Number | The call id

var opts = { 
  'callAnnotation': new BehavioralsignalsUapiClient.RequestCallAnnotationModel() // RequestCallAnnotationModel | Annotations
};
apiInstance.updateCallAnnotation(callId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callId** | **Number**| The call id | 
 **callAnnotation** | [**RequestCallAnnotationModel**](RequestCallAnnotationModel.md)| Annotations | [optional] 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCallLock"></a>
# **updateCallLock**
> SuccessModel updateCallLock(id)

Locks or updates the annotation lock of a call

Locks or updates the annotation lock of a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AnnotationsApi();

var id = 56; // Number | The call id

apiInstance.updateCallLock(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The call id | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

