# BehavioralsignalsUapiClient.ResponseHighlightsFrame

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**index** | **Number** |  | 
**intensity** | **Number** |  | 
**role** | **String** |  | [optional] 
**ts** | **String** |  | 


<a name="RoleEnum"></a>
## Enum: RoleEnum


* `Agent` (value: `"Agent"`)

* `Customer` (value: `"Customer"`)

* `Overlap` (value: `"Overlap"`)




