# BehavioralsignalsUapiClient.ResponseBookmarkCategoriesList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ResponseBookmarkCategory]**](ResponseBookmarkCategory.md) |  | [optional] 


