# BehavioralsignalsUapiClient.EmployeesApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getEmployeeDetails**](EmployeesApi.md#getEmployeeDetails) | **GET** /employees/{employee_id}/ | Returns details for employee with id employee_id.
[**getEmployeesList**](EmployeesApi.md#getEmployeesList) | **GET** /employees/ | Returns list of tenant employees. If param role is provided returns list of all employees with the specific role. If param campaign_id is provided returns list of all employees role agent that belong to the campaign.


<a name="getEmployeeDetails"></a>
# **getEmployeeDetails**
> Employee getEmployeeDetails(employeeId)

Returns details for employee with id employee_id.

Returns details for employee with id employee_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.EmployeesApi();

var employeeId = "employeeId_example"; // String | The employee id

apiInstance.getEmployeeDetails(employeeId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **employeeId** | **String**| The employee id | 

### Return type

[**Employee**](Employee.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getEmployeesList"></a>
# **getEmployeesList**
> ResponseEmployeesList getEmployeesList(opts)

Returns list of tenant employees. If param role is provided returns list of all employees with the specific role. If param campaign_id is provided returns list of all employees role agent that belong to the campaign.

Returns a list of employees

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.EmployeesApi();

var opts = { 
  'role': "role_example", // String | The employee role
  'page': 8.14, // Number | Page number
  'limit': 8.14, // Number | The maximum size of the page entries
  'campaignId': "campaignId_example" // String | Id of a campaign in which an employee with role agent belongs to
};
apiInstance.getEmployeesList(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **role** | **String**| The employee role | [optional] 
 **page** | **Number**| Page number | [optional] 
 **limit** | **Number**| The maximum size of the page entries | [optional] 
 **campaignId** | **String**| Id of a campaign in which an employee with role agent belongs to | [optional] 

### Return type

[**ResponseEmployeesList**](ResponseEmployeesList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

