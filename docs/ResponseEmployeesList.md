# BehavioralsignalsUapiClient.ResponseEmployeesList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[Employee]**](Employee.md) |  | [optional] 


