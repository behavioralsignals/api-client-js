# BehavioralsignalsUapiClient.CampaignsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCampaign**](CampaignsApi.md#createCampaign) | **POST** /campaigns/ | Create a new campaign
[**deleteCampaign**](CampaignsApi.md#deleteCampaign) | **DELETE** /campaigns/{id}/ | Delete a campaign
[**getAggregatedCampaign**](CampaignsApi.md#getAggregatedCampaign) | **GET** /aggregator/campaigns/{id}/ | Returns aggregations of a campaign.
[**getAggregatedCampaigns**](CampaignsApi.md#getAggregatedCampaigns) | **GET** /aggregator/campaigns/ | Returns list of campaigns aggregations.
[**getCampaign**](CampaignsApi.md#getCampaign) | **GET** /campaigns/{id}/ | Get a campaign
[**getCampaigns**](CampaignsApi.md#getCampaigns) | **GET** /campaigns/ | 
[**updateCampaign**](CampaignsApi.md#updateCampaign) | **PUT** /campaigns/{id}/ | Update a campaign


<a name="createCampaign"></a>
# **createCampaign**
> CreationModel createCampaign(campaignRequest)

Create a new campaign

Create a new campaign

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var campaignRequest = new BehavioralsignalsUapiClient.Campaign(); // Campaign | A campaign request object

apiInstance.createCampaign(campaignRequest).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **campaignRequest** | [**Campaign**](Campaign.md)| A campaign request object | 

### Return type

[**CreationModel**](CreationModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteCampaign"></a>
# **deleteCampaign**
> SuccessModel deleteCampaign(id)

Delete a campaign

Deletes a campaign

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var id = 56; // Number | The campaign id

apiInstance.deleteCampaign(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The campaign id | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAggregatedCampaign"></a>
# **getAggregatedCampaign**
> ResponseAggregatedCampaign getAggregatedCampaign(id, opts)

Returns aggregations of a campaign.

Returns aggregations for campaign identified by id .

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var id = 56; // Number | The campaign id

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the end of a date range
  'campaignRequest': new BehavioralsignalsUapiClient.CampaignRequest() // CampaignRequest | Campaign data
};
apiInstance.getAggregatedCampaign(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The campaign id | 
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **startDate** | **Date**| Aggregates campaign based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaign based on the end of a date range | [optional] 
 **campaignRequest** | [**CampaignRequest**](CampaignRequest.md)| Campaign data | [optional] 

### Return type

[**ResponseAggregatedCampaign**](ResponseAggregatedCampaign.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedCampaigns"></a>
# **getAggregatedCampaigns**
> ResponseAggregatedCampaigns getAggregatedCampaigns(opts)

Returns list of campaigns aggregations.

Returns list of campaigns aggregations.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the end of a date range
  'campaignRequest': new BehavioralsignalsUapiClient.CampaignRequest() // CampaignRequest | Campaign request data
};
apiInstance.getAggregatedCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **startDate** | **Date**| Aggregates campaigns based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaigns based on the end of a date range | [optional] 
 **campaignRequest** | [**CampaignRequest**](CampaignRequest.md)| Campaign request data | [optional] 

### Return type

[**ResponseAggregatedCampaigns**](ResponseAggregatedCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getCampaign"></a>
# **getCampaign**
> Campaign getCampaign(id)

Get a campaign

Get a campaign

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var id = 56; // Number | The campaign id

apiInstance.getCampaign(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The campaign id | 

### Return type

[**Campaign**](Campaign.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getCampaigns"></a>
# **getCampaigns**
> ResponseCampaigns getCampaigns(opts)



Returns a list of campaigns without aggregations

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var opts = { 
  'page': 8.14, // Number | Page number
  'limit': 8.14 // Number | The maximum size of the page entries
};
apiInstance.getCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Page number | [optional] 
 **limit** | **Number**| The maximum size of the page entries | [optional] 

### Return type

[**ResponseCampaigns**](ResponseCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCampaign"></a>
# **updateCampaign**
> SuccessModel updateCampaign(id, campaignRequest)

Update a campaign

Update a campaign

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignsApi();

var id = 56; // Number | The campaign id

var campaignRequest = new BehavioralsignalsUapiClient.Campaign(); // Campaign | A campaign request object

apiInstance.updateCampaign(id, campaignRequest).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The campaign id | 
 **campaignRequest** | [**Campaign**](Campaign.md)| A campaign request object | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

