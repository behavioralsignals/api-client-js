# BehavioralsignalsUapiClient.ConfirmationApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**confirmAccount**](ConfirmationApi.md#confirmAccount) | **GET** /accounts/confirm/ | Confirm subscription for the new account


<a name="confirmAccount"></a>
# **confirmAccount**
> confirmAccount(opts)

Confirm subscription for the new account

Confirm subscription for the new account

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.ConfirmationApi();

var opts = { 
  'token': "token_example" // String | The expiration token used for subscription validation
};
apiInstance.confirmAccount(opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**| The expiration token used for subscription validation | [optional] 

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

