# BehavioralsignalsUapiClient.ResponseAggregatedLegacyCampaigns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ResponseAggregatedLegacyCampaign]**](ResponseAggregatedLegacyCampaign.md) |  | [optional] 


