# BehavioralsignalsUapiClient.ResponseCampaignAggregations

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**company** | [**Company**](Company.md) |  | [optional] 
**startdate** | **Date** | Start date is the date of the first call in current aggregation slice | [optional] 
**enddate** | **Date** | End date is the date of the lst call in current aggregation slice | [optional] 


