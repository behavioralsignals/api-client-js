# BehavioralsignalsUapiClient.JobMediaMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **Number** | The duration of the call in miliseconds | [optional] 
**channels** | **Number** | The number of channels on recording | [optional] 


