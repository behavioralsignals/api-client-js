# BehavioralsignalsUapiClient.AccountsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**subscribeAccount**](AccountsApi.md#subscribeAccount) | **POST** /accounts/subscribe/ | Subscribes for a new account


<a name="subscribeAccount"></a>
# **subscribeAccount**
> subscribeAccount(subscriptionRequest)

Subscribes for a new account

Subscribes for a new account

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AccountsApi();

var subscriptionRequest = new BehavioralsignalsUapiClient.AccountSubscription(); // AccountSubscription | Subscription object

apiInstance.subscribeAccount(subscriptionRequest).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subscriptionRequest** | [**AccountSubscription**](AccountSubscription.md)| Subscription object | 

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

