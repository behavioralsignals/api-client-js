# BehavioralsignalsUapiClient.ResponseUserList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[UserDetails]**](UserDetails.md) |  | [optional] 


