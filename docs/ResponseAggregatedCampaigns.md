# BehavioralsignalsUapiClient.ResponseAggregatedCampaigns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[ResponseAggregatedCampaign]**](ResponseAggregatedCampaign.md) |  | [optional] 


