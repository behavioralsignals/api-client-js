# BehavioralsignalsUapiClient.IndicatorType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** | The label of the indicator type | 
**percentValue** | **Number** | Type&#39;s score presented by percentage | 


