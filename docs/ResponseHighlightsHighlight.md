# BehavioralsignalsUapiClient.ResponseHighlightsHighlight

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | 
**text** | **String** |  | [optional] 
**st** | **Number** |  | 
**et** | **Number** |  | 
**notes** | [**[ResponseHighlightNote]**](ResponseHighlightNote.md) |  | [optional] 


