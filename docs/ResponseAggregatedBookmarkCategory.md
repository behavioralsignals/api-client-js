# BehavioralsignalsUapiClient.ResponseAggregatedBookmarkCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agentTime** | **Number** | Agent talk time in seconds | [optional] 
**customerTime** | **Number** | Customer talk time in seconds | [optional] 
**overlapTime** | **Number** | Overlap time in seconds | [optional] 
**customerPositivityNegative** | **Number** | Customer negative positivity KPI | [optional] 
**customerPositivityNeutral** | **Number** | Customer neutral positivity KPI | [optional] 
**customerPositivityPositive** | **Number** | Customer positive positivity KPI | [optional] 
**customerPositivityTrend** | **Number** | Customer positivity trend | [optional] 


