# BehavioralsignalsUapiClient.Call

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique identifier for the call | [optional] 
**title** | **String** | A short descriptive title indicating the highlight of the call | [optional] 
**duration** | **Number** | The duration of the call in seconds | [optional] 


