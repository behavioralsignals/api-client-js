# BehavioralsignalsUapiClient.AggregationsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAggregatedAgents**](AggregationsApi.md#getAggregatedAgents) | **GET** /aggregator/agents/ | Returns a list of all agents
[**getAggregatedCalls**](AggregationsApi.md#getAggregatedCalls) | **GET** /aggregator/calls/ | Returns aggregations for a group of calls.
[**getAggregatedCampaign**](AggregationsApi.md#getAggregatedCampaign) | **GET** /aggregator/campaigns/{id}/ | Returns aggregations of a campaign.
[**getAggregatedCampaigns**](AggregationsApi.md#getAggregatedCampaigns) | **GET** /aggregator/campaigns/ | Returns list of campaigns aggregations.
[**getAggregatedLegacyCampaign**](AggregationsApi.md#getAggregatedLegacyCampaign) | **GET** /aggregator/campaignslegacy/{campaign_id}/ | Returns details for a legacy campaign.
[**getAggregatedLegacyCampaigns**](AggregationsApi.md#getAggregatedLegacyCampaigns) | **GET** /aggregator/campaignslegacy/ | Returns an aggregated list of legacy campaigns.


<a name="getAggregatedAgents"></a>
# **getAggregatedAgents**
> ResponseAggregatedAgents getAggregatedAgents(opts)

Returns a list of all agents

Returns a list of all agents

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var opts = { 
  'sortBy': "size", // String | Sorting option
  'campaignId': 56, // Number | Filter calls by campaign and calculate aggregates per agent
  'legacycampaignId': "legacycampaignId_example", // String | Filter calls by legacy campaign and calculate aggregates per agent
  'page': 1, // Number | Page number
  'limit': 10, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates agents based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates agents based on the end of a date range
};
apiInstance.getAggregatedAgents(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **campaignId** | **Number**| Filter calls by campaign and calculate aggregates per agent | [optional] 
 **legacycampaignId** | **String**| Filter calls by legacy campaign and calculate aggregates per agent | [optional] 
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 10]
 **startDate** | **Date**| Aggregates agents based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates agents based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedAgents**](ResponseAggregatedAgents.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedCalls"></a>
# **getAggregatedCalls**
> ResponseAggregatedCalls getAggregatedCalls(opts)

Returns aggregations for a group of calls.

Returns aggregations for call group selected by given criteria.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var opts = { 
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'callGroup': new BehavioralsignalsUapiClient.CallGroup() // CallGroup | Call group data
};
apiInstance.getAggregatedCalls(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **callGroup** | [**CallGroup**](CallGroup.md)| Call group data | [optional] 

### Return type

[**ResponseAggregatedCalls**](ResponseAggregatedCalls.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedCampaign"></a>
# **getAggregatedCampaign**
> ResponseAggregatedCampaign getAggregatedCampaign(id, opts)

Returns aggregations of a campaign.

Returns aggregations for campaign identified by id .

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var id = 56; // Number | The campaign id

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the end of a date range
  'campaignRequest': new BehavioralsignalsUapiClient.CampaignRequest() // CampaignRequest | Campaign data
};
apiInstance.getAggregatedCampaign(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The campaign id | 
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **startDate** | **Date**| Aggregates campaign based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaign based on the end of a date range | [optional] 
 **campaignRequest** | [**CampaignRequest**](CampaignRequest.md)| Campaign data | [optional] 

### Return type

[**ResponseAggregatedCampaign**](ResponseAggregatedCampaign.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedCampaigns"></a>
# **getAggregatedCampaigns**
> ResponseAggregatedCampaigns getAggregatedCampaigns(opts)

Returns list of campaigns aggregations.

Returns list of campaigns aggregations.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the end of a date range
  'campaignRequest': new BehavioralsignalsUapiClient.CampaignRequest() // CampaignRequest | Campaign request data
};
apiInstance.getAggregatedCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **startDate** | **Date**| Aggregates campaigns based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaigns based on the end of a date range | [optional] 
 **campaignRequest** | [**CampaignRequest**](CampaignRequest.md)| Campaign request data | [optional] 

### Return type

[**ResponseAggregatedCampaigns**](ResponseAggregatedCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedLegacyCampaign"></a>
# **getAggregatedLegacyCampaign**
> ResponseAggregatedLegacyCampaign getAggregatedLegacyCampaign(campaignId, opts)

Returns details for a legacy campaign.

Returns details for a legacy campaign.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var campaignId = "campaignId_example"; // String | The campaign id

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates campaign based on the end of a date range
};
apiInstance.getAggregatedLegacyCampaign(campaignId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **campaignId** | **String**| The campaign id | 
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **startDate** | **Date**| Aggregates campaign based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaign based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedLegacyCampaign**](ResponseAggregatedLegacyCampaign.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedLegacyCampaigns"></a>
# **getAggregatedLegacyCampaigns**
> ResponseAggregatedLegacyCampaigns getAggregatedLegacyCampaigns(opts)

Returns an aggregated list of legacy campaigns.

Returns a legacy campaign aggregated list

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AggregationsApi();

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates campaigns based on the end of a date range
};
apiInstance.getAggregatedLegacyCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **startDate** | **Date**| Aggregates campaigns based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaigns based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedLegacyCampaigns**](ResponseAggregatedLegacyCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

