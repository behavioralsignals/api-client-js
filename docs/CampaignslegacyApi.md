# BehavioralsignalsUapiClient.CampaignslegacyApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAggregatedLegacyCampaign**](CampaignslegacyApi.md#getAggregatedLegacyCampaign) | **GET** /aggregator/campaignslegacy/{campaign_id}/ | Returns details for a legacy campaign.
[**getAggregatedLegacyCampaigns**](CampaignslegacyApi.md#getAggregatedLegacyCampaigns) | **GET** /aggregator/campaignslegacy/ | Returns an aggregated list of legacy campaigns.
[**getLegacyCampaigns**](CampaignslegacyApi.md#getLegacyCampaigns) | **GET** /campaignslegacy/ | 


<a name="getAggregatedLegacyCampaign"></a>
# **getAggregatedLegacyCampaign**
> ResponseAggregatedLegacyCampaign getAggregatedLegacyCampaign(campaignId, opts)

Returns details for a legacy campaign.

Returns details for a legacy campaign.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignslegacyApi();

var campaignId = "campaignId_example"; // String | The campaign id

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaign based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates campaign based on the end of a date range
};
apiInstance.getAggregatedLegacyCampaign(campaignId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **campaignId** | **String**| The campaign id | 
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **startDate** | **Date**| Aggregates campaign based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaign based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedLegacyCampaign**](ResponseAggregatedLegacyCampaign.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedLegacyCampaigns"></a>
# **getAggregatedLegacyCampaigns**
> ResponseAggregatedLegacyCampaigns getAggregatedLegacyCampaigns(opts)

Returns an aggregated list of legacy campaigns.

Returns a legacy campaign aggregated list

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignslegacyApi();

var opts = { 
  'agentId': "agentId_example", // String | Filter calls based on agent_id and calculate per campaign aggregates
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates campaigns based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates campaigns based on the end of a date range
};
apiInstance.getAggregatedLegacyCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| Filter calls based on agent_id and calculate per campaign aggregates | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **startDate** | **Date**| Aggregates campaigns based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates campaigns based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedLegacyCampaigns**](ResponseAggregatedLegacyCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getLegacyCampaigns"></a>
# **getLegacyCampaigns**
> ResponseLegacyCampaigns getLegacyCampaigns(opts)



Returns a list of legacy campaigns without aggregation

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CampaignslegacyApi();

var opts = { 
  'page': 8.14, // Number | Page number
  'limit': 8.14 // Number | The maximum size of the page entries
};
apiInstance.getLegacyCampaigns(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Page number | [optional] 
 **limit** | **Number**| The maximum size of the page entries | [optional] 

### Return type

[**ResponseLegacyCampaigns**](ResponseLegacyCampaigns.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

