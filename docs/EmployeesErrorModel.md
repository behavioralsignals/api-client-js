# BehavioralsignalsUapiClient.EmployeesErrorModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** | Http status code | [optional] 
**message** | **String** | A message describes the error | [optional] 


