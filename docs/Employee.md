# BehavioralsignalsUapiClient.Employee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**employeeId** | **String** |  | 
**name** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**gender** | **String** |  | [optional] 
**age** | **Number** |  | [optional] 
**yearsEmployed** | **Number** |  | [optional] 
**monthsEmployed** | **Number** |  | [optional] 
**employeeRole** | **String** |  | [optional] 


<a name="GenderEnum"></a>
## Enum: GenderEnum


* `male` (value: `"male"`)

* `female` (value: `"female"`)

* `unknown` (value: `"unknown"`)




<a name="EmployeeRoleEnum"></a>
## Enum: EmployeeRoleEnum


* `agent` (value: `"agent"`)

* `analyst` (value: `"analyst"`)

* `reviewer` (value: `"reviewer"`)




