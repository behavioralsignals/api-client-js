# BehavioralsignalsUapiClient.JobJobStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** | The status of the job in the service | [optional] 
**statusmsg** | **String** | Description of the status of a job in service | [optional] 
**servicePid** | **Number** | The corresponding process id of a job in service | [optional] 


