# BehavioralsignalsUapiClient.UserDetailsDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gender** | **String** |  | [optional] 
**age** | **Number** |  | [optional] 
**yearsEmployed** | **Number** |  | [optional] 
**monthsEmployed** | **Number** |  | [optional] 
**role** | **String** |  | [optional] 
**employeeId** | **String** |  | [optional] 
**imported** | **Boolean** | Whether the user has access to the call data | [optional] [default to false]
**notified** | **Boolean** | Whether the user has been made aware of specific changes in their login state | [optional] [default to true]
**origin** | **String** | The origin app of the user | [optional] 


<a name="GenderEnum"></a>
## Enum: GenderEnum


* `male` (value: `"male"`)

* `female` (value: `"female"`)

* `unknown` (value: `"unknown"`)




