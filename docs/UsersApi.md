# BehavioralsignalsUapiClient.UsersApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserDetails**](UsersApi.md#getUserDetails) | **GET** /users/{user_id}/ | Returns details for user with id user_id depending on permissions. If has admin permissions returns details for any user else returns only his own details.
[**getUserList**](UsersApi.md#getUserList) | **GET** /users/ | Returns list of users details depending on permissions. if user returns his own details if admin returns list of details for all users.
[**registerUser**](UsersApi.md#registerUser) | **POST** /users/register/ | Registers a new user
[**updateUserNotify**](UsersApi.md#updateUserNotify) | **PUT** /me/notify/ | Updates the notified property of the user


<a name="getUserDetails"></a>
# **getUserDetails**
> UserDetails getUserDetails(userId)

Returns details for user with id user_id depending on permissions. If has admin permissions returns details for any user else returns only his own details.

Returns user details.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.UsersApi();

var userId = 56; // Number | The user id

apiInstance.getUserDetails(userId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userId** | **Number**| The user id | 

### Return type

[**UserDetails**](UserDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getUserList"></a>
# **getUserList**
> ResponseUserList getUserList(opts)

Returns list of users details depending on permissions. if user returns his own details if admin returns list of details for all users.

Returns a list of users or user details.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.UsersApi();

var opts = { 
  'page': 1, // Number | Page number
  'limit': 10 // Number | The maximum size of the page entries
};
apiInstance.getUserList(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 10]

### Return type

[**ResponseUserList**](ResponseUserList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="registerUser"></a>
# **registerUser**
> UserCreation registerUser(user)

Registers a new user

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.UsersApi();

var user = new BehavioralsignalsUapiClient.UserDetails(); // UserDetails | The user to create

apiInstance.registerUser(user).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **user** | [**UserDetails**](UserDetails.md)| The user to create | 

### Return type

[**UserCreation**](UserCreation.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateUserNotify"></a>
# **updateUserNotify**
> SuccessModel updateUserNotify(notifyFlag)

Updates the notified property of the user

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.UsersApi();

var notifyFlag = new BehavioralsignalsUapiClient.UserNotificationDetails(); // UserNotificationDetails | The notification flag object

apiInstance.updateUserNotify(notifyFlag).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notifyFlag** | [**UserNotificationDetails**](UserNotificationDetails.md)| The notification flag object | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

