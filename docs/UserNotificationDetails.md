# BehavioralsignalsUapiClient.UserNotificationDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**notified** | **Boolean** | The notification flag | [optional] 


