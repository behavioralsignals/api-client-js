# BehavioralsignalsUapiClient.AnnotationHighlight

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | [optional] 
**text** | **String** |  | [optional] 
**st** | **Number** |  | 
**et** | **Number** |  | 


