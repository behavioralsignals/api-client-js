# BehavioralsignalsUapiClient.Company

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique identifier | [optional] 
**name** | **String** | The company name | [optional] 


