# BehavioralsignalsUapiClient.ResponseHighlightsCustomer

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **String** | ISO 639-1 Code (e.g., en) | [optional] 
**gender** | **String** |  | [optional] 
**ageGroup** | **String** |  | [optional] 
**customerNumber** | **String** |  | [optional] 


<a name="LanguageEnum"></a>
## Enum: LanguageEnum


* `en` (value: `"en"`)

* `sp` (value: `"sp"`)




<a name="GenderEnum"></a>
## Enum: GenderEnum


* `Male` (value: `"Male"`)

* `Female` (value: `"Female"`)




<a name="AgeGroupEnum"></a>
## Enum: AgeGroupEnum


* `Young` (value: `"Young"`)

* `Adult` (value: `"Adult"`)

* `Elder` (value: `"Elder"`)




