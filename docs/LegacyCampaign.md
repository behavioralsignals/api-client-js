# BehavioralsignalsUapiClient.LegacyCampaign

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | String identifier for the campaign | [optional] 
**title** | **String** | The campaign title | [optional] 


