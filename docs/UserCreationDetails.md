# BehavioralsignalsUapiClient.UserCreationDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gender** | **String** |  | [optional] 
**age** | **Number** |  | [optional] 
**yearsEmployed** | **Number** |  | [optional] 
**monthsEmployed** | **Number** |  | [optional] 
**role** | **String** |  | [optional] 
**employeeId** | **String** |  | [optional] 


<a name="GenderEnum"></a>
## Enum: GenderEnum


* `male` (value: `"male"`)

* `female` (value: `"female"`)

* `unknown` (value: `"unknown"`)




