# BehavioralsignalsUapiClient.RequestCallAnnotationModelCallAnnotation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**annotations** | [**RequestAnnotationsModel**](RequestAnnotationsModel.md) |  | 
**comments** | [**RequestAnnotationComments**](RequestAnnotationComments.md) |  | 
**callRetrieveTimestamp** | **String** | Timestamp that the call was retrieved by the client | [optional] 


