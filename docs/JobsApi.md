# BehavioralsignalsUapiClient.JobsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getJobAudioStream**](JobsApi.md#getJobAudioStream) | **GET** /jobs/{job_id}/streamaudio/ | Get stream audio for job with id job_id
[**getJobHighlights**](JobsApi.md#getJobHighlights) | **GET** /jobs/{job_id}/highlights/ | Get highlight results for job with id job_id
[**getJobJobId**](JobsApi.md#getJobJobId) | **GET** /jobs/{job_id}/ | Returns details for job with id job_id.
[**getJobResults**](JobsApi.md#getJobResults) | **GET** /jobs/{job_id}/results/ | Get call, frame, asr or highlight results for job with id job_id
[**getJobs**](JobsApi.md#getJobs) | **GET** /jobs/ | Returns a list of jobs.
[**postJob**](JobsApi.md#postJob) | **POST** /jobs/create/ | Create a new Job
[**postJobCreate**](JobsApi.md#postJobCreate) | **POST** /jobs/create/process/{process_id} | Creates new job from service process id.
[**uploadJobBundle**](JobsApi.md#uploadJobBundle) | **POST** /jobs/upload/ | Upload a new job bundle


<a name="getJobAudioStream"></a>
# **getJobAudioStream**
> ResponseStreamAudio getJobAudioStream(jobId)

Get stream audio for job with id job_id

Get stream audio for job with id job_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var jobId = 56; // Number | The Job id

apiInstance.getJobAudioStream(jobId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| The Job id | 

### Return type

[**ResponseStreamAudio**](ResponseStreamAudio.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: audio/x-wav

<a name="getJobHighlights"></a>
# **getJobHighlights**
> ResponseHighlightsCall getJobHighlights(jobId)

Get highlight results for job with id job_id

Get highlight results for job with id job_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var jobId = 56; // Number | The Job id

apiInstance.getJobHighlights(jobId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| The Job id | 

### Return type

[**ResponseHighlightsCall**](ResponseHighlightsCall.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getJobJobId"></a>
# **getJobJobId**
> Job getJobJobId(jobId)

Returns details for job with id job_id.

Returns details for job with id job_id.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var jobId = 56; // Number | The job id

apiInstance.getJobJobId(jobId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| The job id | 

### Return type

[**Job**](Job.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getJobResults"></a>
# **getJobResults**
> getJobResults(jobId, level, opts)

Get call, frame, asr or highlight results for job with id job_id

Get call, frame, asr or highlight results for job with id job_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var jobId = 56; // Number | The Job id

var level = "level_example"; // String | The level param, level={call, frame, gcharts, asr, highlights}

var opts = { 
  'sendAllFrames': 56 // Number | if not zero, all frames from framelevel results are returned
};
apiInstance.getJobResults(jobId, level, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **jobId** | **Number**| The Job id | 
 **level** | **String**| The level param, level&#x3D;{call, frame, gcharts, asr, highlights} | 
 **sendAllFrames** | **Number**| if not zero, all frames from framelevel results are returned | [optional] 

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getJobs"></a>
# **getJobs**
> ResponseJobList getJobs(opts)

Returns a list of jobs.

Returns a list of jobs.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var opts = { 
  'page': 1, // Number | Page number
  'limit': 10 // Number | The maximum size of the page entries
};
apiInstance.getJobs(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 10]

### Return type

[**ResponseJobList**](ResponseJobList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postJob"></a>
# **postJob**
> JobCreationModel postJob(filename, channels, opts)

Create a new Job

Create a new job

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var filename = "filename_example"; // String | Name of the audio file recording

var channels = 56; // Number | Number of channels in audio file recording

var opts = { 
  'file': "/path/to/file.txt" // File | The file to upload.
};
apiInstance.postJob(filename, channels, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filename** | **String**| Name of the audio file recording | 
 **channels** | **Number**| Number of channels in audio file recording | 
 **file** | **File**| The file to upload. | [optional] 

### Return type

[**JobCreationModel**](JobCreationModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="postJobCreate"></a>
# **postJobCreate**
> postJobCreate(processId)

Creates new job from service process id.

Creates new job from service process id.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var processId = 56; // Number | The service process id

apiInstance.postJobCreate(processId).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **processId** | **Number**| The service process id | 

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="uploadJobBundle"></a>
# **uploadJobBundle**
> CreationModel uploadJobBundle(opts)

Upload a new job bundle

Upload a new job bundle

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.JobsApi();

var opts = { 
  'file': "/path/to/file.txt" // File | The file to upload
};
apiInstance.uploadJobBundle(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **File**| The file to upload | [optional] 

### Return type

[**CreationModel**](CreationModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

