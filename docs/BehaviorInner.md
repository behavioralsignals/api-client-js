# BehavioralsignalsUapiClient.BehaviorInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** | The label describing the behavior class | [optional] 
**value** | [**BehaviorInnerValue**](BehaviorInnerValue.md) |  | [optional] 


