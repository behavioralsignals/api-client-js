# BehavioralsignalsUapiClient.CallDetailsBehaviors

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** | The key label of the Behavior | [optional] 
**value** | [**Behavior**](Behavior.md) |  | [optional] 


