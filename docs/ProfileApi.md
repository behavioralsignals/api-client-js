# BehavioralsignalsUapiClient.ProfileApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**updateUserNotify**](ProfileApi.md#updateUserNotify) | **PUT** /me/notify/ | Updates the notified property of the user


<a name="updateUserNotify"></a>
# **updateUserNotify**
> SuccessModel updateUserNotify(notifyFlag)

Updates the notified property of the user

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.ProfileApi();

var notifyFlag = new BehavioralsignalsUapiClient.UserNotificationDetails(); // UserNotificationDetails | The notification flag object

apiInstance.updateUserNotify(notifyFlag).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **notifyFlag** | [**UserNotificationDetails**](UserNotificationDetails.md)| The notification flag object | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

