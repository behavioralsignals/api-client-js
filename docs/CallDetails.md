# BehavioralsignalsUapiClient.CallDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agentId** | **String** | The agent identification string assigned to the call | [optional] 
**agentName** | **String** | The agent name | [optional] 
**datetime** | **Date** | Datetime of the call | [optional] 
**agentTime** | **Number** | Percentage of agent&#39;s talk time | [optional] 
**customerTime** | **Number** | Percentage of customer&#39;s talk time | [optional] 
**overlapTime** | **Number** | Percentage of overlap time | [optional] 
**customerNumber** | **String** | Customer conntact number | [optional] 
**callDirection** | **String** | Designates whether a call is inbound or outbound | [optional] 
**behaviors** | [**[CallDetailsBehaviors]**](CallDetailsBehaviors.md) |  | [optional] 
**trends** | [**CallDetailsTrends**](CallDetailsTrends.md) |  | [optional] 


