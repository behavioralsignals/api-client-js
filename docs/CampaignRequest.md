# BehavioralsignalsUapiClient.CampaignRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | **[String]** |  | [optional] 
**id** | **Number** | The id of the campaign | [optional] 
**campaignId** | **String** |  | [optional] 
**startDate** | **Date** |  | [optional] 
**endDate** | **Date** |  | [optional] 


