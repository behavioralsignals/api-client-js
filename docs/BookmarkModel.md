# BehavioralsignalsUapiClient.BookmarkModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bookmarkcategoryId** | **Number** | The id of the bookmark category | [optional] 
**callId** | **Number** | The id of the call | [optional] 


