# BehavioralsignalsUapiClient.SuccessModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Number** | Http status code | [optional] 
**message** | **String** | A message that describes what happened | [optional] 


