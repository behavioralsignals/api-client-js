# BehavioralsignalsUapiClient.CreateTenant

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Unique tenant name on backend | 
**tenantId** | **String** | Unique tenant identifier on backend | 


