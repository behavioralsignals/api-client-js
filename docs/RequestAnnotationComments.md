# BehavioralsignalsUapiClient.RequestAnnotationComments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent** | [**AnnotationCommentsHighlights**](AnnotationCommentsHighlights.md) |  | 
**customer** | [**AnnotationCommentsHighlights**](AnnotationCommentsHighlights.md) |  | 


