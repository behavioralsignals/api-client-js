# BehavioralsignalsUapiClient.ProcessesApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProcessAudioStream**](ProcessesApi.md#getProcessAudioStream) | **GET** /processes/{process_id}/streamaudio/ | Get stream audio for process with id process_id
[**getProcessResults**](ProcessesApi.md#getProcessResults) | **GET** /processes/{process_id}/results/ | Get call, frame, asr or highlight results for process with id process_id


<a name="getProcessAudioStream"></a>
# **getProcessAudioStream**
> ResponseStreamAudio getProcessAudioStream(processId)

Get stream audio for process with id process_id

Get stream audio for process with id process_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.ProcessesApi();

var processId = 56; // Number | The service process id

apiInstance.getProcessAudioStream(processId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **processId** | **Number**| The service process id | 

### Return type

[**ResponseStreamAudio**](ResponseStreamAudio.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: audio/x-wav, audio/mpeg, application/json

<a name="getProcessResults"></a>
# **getProcessResults**
> getProcessResults(processId, opts)

Get call, frame, asr or highlight results for process with id process_id

Get call, frame, asr or highlight results for process with id process_id

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.ProcessesApi();

var processId = 56; // Number | The service process id

var opts = { 
  'level': "level_example", // String | The level param, level={call, frame, gcharts, asr, highlights}
  'sendAllFrames': 56 // Number | if not zero, all frames from framelevel results are returned
};
apiInstance.getProcessResults(processId, opts).then(function() {
  console.log('API called successfully.');
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **processId** | **Number**| The service process id | 
 **level** | **String**| The level param, level&#x3D;{call, frame, gcharts, asr, highlights} | [optional] 
 **sendAllFrames** | **Number**| if not zero, all frames from framelevel results are returned | [optional] 

### Return type

null (empty response body)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

