# BehavioralsignalsUapiClient.AgentsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAgents**](AgentsApi.md#getAgents) | **GET** /agents/ | 
[**getAggregatedAgents**](AgentsApi.md#getAggregatedAgents) | **GET** /aggregator/agents/ | Returns a list of all agents
[**getAggregatedAgentsAgentId**](AgentsApi.md#getAggregatedAgentsAgentId) | **GET** /aggregator/agents/{agent_id}/ | Returns a list of all agents


<a name="getAgents"></a>
# **getAgents**
> ResponseAgents getAgents()



Returns a list of agents without aggregation

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AgentsApi();
apiInstance.getAgents().then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ResponseAgents**](ResponseAgents.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAggregatedAgents"></a>
# **getAggregatedAgents**
> ResponseAggregatedAgents getAggregatedAgents(opts)

Returns a list of all agents

Returns a list of all agents

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AgentsApi();

var opts = { 
  'sortBy': "size", // String | Sorting option
  'campaignId': 56, // Number | Filter calls by campaign and calculate aggregates per agent
  'legacycampaignId': "legacycampaignId_example", // String | Filter calls by legacy campaign and calculate aggregates per agent
  'page': 1, // Number | Page number
  'limit': 10, // Number | The maximum size of the page entries
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates agents based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates agents based on the end of a date range
};
apiInstance.getAggregatedAgents(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **campaignId** | **Number**| Filter calls by campaign and calculate aggregates per agent | [optional] 
 **legacycampaignId** | **String**| Filter calls by legacy campaign and calculate aggregates per agent | [optional] 
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 10]
 **startDate** | **Date**| Aggregates agents based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates agents based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedAgents**](ResponseAggregatedAgents.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getAggregatedAgentsAgentId"></a>
# **getAggregatedAgentsAgentId**
> ResponseAggregatedAgent getAggregatedAgentsAgentId(agentId, opts)

Returns a list of all agents

Returns a list of all agents

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.AgentsApi();

var agentId = "agentId_example"; // String | The agent id

var opts = { 
  'campaignId': 56, // Number | Filter calls by campaign and calculate aggregates per agent
  'legacycampaignId': "legacycampaignId_example", // String | Filter calls by legacy campaign and calculate aggregates per agent
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Aggregates agent based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00") // Date | Aggregates agent based on the end of a date range
};
apiInstance.getAggregatedAgentsAgentId(agentId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| The agent id | 
 **campaignId** | **Number**| Filter calls by campaign and calculate aggregates per agent | [optional] 
 **legacycampaignId** | **String**| Filter calls by legacy campaign and calculate aggregates per agent | [optional] 
 **startDate** | **Date**| Aggregates agent based on the start of a date range | [optional] 
 **endDate** | **Date**| Aggregates agent based on the end of a date range | [optional] 

### Return type

[**ResponseAggregatedAgent**](ResponseAggregatedAgent.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

