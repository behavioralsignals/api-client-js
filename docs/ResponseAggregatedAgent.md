# BehavioralsignalsUapiClient.ResponseAggregatedAgent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Unique identifier for the agent | [optional] 
**profilePic** | **String** | A source to the agent&#39;s profile picture | [optional] 
**name** | **String** | The agent name | [optional] 


