# BehavioralsignalsUapiClient.CallDetailsTrends

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**customerPositivity** | **Number** | Customer positivity KPI | [optional] 


