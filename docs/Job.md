# BehavioralsignalsUapiClient.Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**jobId** | **Number** | Unique identifier for the job | [optional] 
**filename** | **String** | Name of the call recording | [optional] 
**batchId** | **String** | Unique identifier or a batch of calls | [optional] 
**uploaded** | **String** | Unique identifier or a batch of calls | [optional] 
**storedata** | **Number** | Unique identifier for the job | [optional] 
**uri** | **String** | Unique identifier or a batch of calls | [optional] 
**mediaMetadata** | [**JobMediaMetadata**](JobMediaMetadata.md) |  | [optional] 
**jobStatus** | [**JobJobStatus**](JobJobStatus.md) |  | [optional] 


