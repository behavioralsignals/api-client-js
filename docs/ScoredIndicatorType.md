# BehavioralsignalsUapiClient.ScoredIndicatorType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**absoluteValue** | **Number** | The absolute number that describes the score | [optional] 
**message** | **String** | A message that describes the output of the score | 


