# BehavioralsignalsUapiClient.ResponseHighlightsSpeaker

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**role** | **String** |  | 
**talkTime** | [**ResponseHighlightsSpeakerTalkTime**](ResponseHighlightsSpeakerTalkTime.md) |  | 
**signalStrength** | **Number** |  | [optional] 
**highlights** | [**[ResponseHighlightsHighlight]**](ResponseHighlightsHighlight.md) |  | 
**kpis** | [**[ResponseHighlightsIndicator]**](ResponseHighlightsIndicator.md) |  | [optional] 
**emotions** | [**[ResponseHighlightsIndicator]**](ResponseHighlightsIndicator.md) |  | 
**behaviors** | [**[ResponseHighlightsBehavior]**](ResponseHighlightsBehavior.md) |  | 


<a name="RoleEnum"></a>
## Enum: RoleEnum


* `Agent` (value: `"Agent"`)

* `Customer` (value: `"Customer"`)




