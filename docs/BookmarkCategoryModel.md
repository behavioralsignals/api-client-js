# BehavioralsignalsUapiClient.BookmarkCategoryModel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of the bookmark category | [optional] 
**title** | **String** | The title of the bookmark category | [optional] 
**desc** | **String** | A short description of the bookmark category | [optional] 


