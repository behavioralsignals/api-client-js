# BehavioralsignalsUapiClient.ResponseHighlightNote

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**userFullname** | **String** | The name and surname of the note user | 
**userPic** | **String** | The source to the profile pic of the note user | [optional] 
**datetime** | **Date** | The datetime of the note | 
**comment** | **String** | The user comment on the highlight | [optional] 


