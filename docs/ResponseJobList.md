# BehavioralsignalsUapiClient.ResponseJobList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[Job]**](Job.md) |  | [optional] 


