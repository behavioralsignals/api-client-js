# BehavioralsignalsUapiClient.ResponseCallsList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[CallDetails]**](CallDetails.md) |  | [optional] 


