# BehavioralsignalsUapiClient.ResponseHighlightsBehavior

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | 
**types** | [**[ResponseHighlightsIndicator]**](ResponseHighlightsIndicator.md) |  | 


