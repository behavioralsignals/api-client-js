# BehavioralsignalsUapiClient.ResponseLegacyCampaigns

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**[LegacyCampaign]**](LegacyCampaign.md) |  | [optional] 


