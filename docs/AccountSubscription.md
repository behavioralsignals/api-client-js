# BehavioralsignalsUapiClient.AccountSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstName** | **String** | Name of the person requesting new account | 
**lastName** | **String** | Last name of the person requesting new account | 
**email** | **String** | Email of the person requesting new account | 
**company** | **String** | Cpmpany the person requesting new account represents | 
**contactNumber** | **String** | Contact number of the person requesting new account | [optional] 


