# BehavioralsignalsUapiClient.UserDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** |  | [optional] 
**username** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**dateJoined** | **String** |  | [optional] 
**lastLogin** | **String** |  | [optional] 
**details** | [**UserDetailsDetails**](UserDetailsDetails.md) |  | [optional] 


