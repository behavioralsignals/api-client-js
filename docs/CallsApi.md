# BehavioralsignalsUapiClient.CallsApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAggregatedCalls**](CallsApi.md#getAggregatedCalls) | **GET** /aggregator/calls/ | Returns aggregations for a group of calls.
[**getCalls**](CallsApi.md#getCalls) | **GET** /calls/ | Returns a list of calls.
[**getCallsCallId**](CallsApi.md#getCallsCallId) | **GET** /calls/{call_id}/ | Returns details for call with id call_id.
[**removeCallLock**](CallsApi.md#removeCallLock) | **DELETE** /calls/{id}/lock/ | Removes the annotation lock of a call
[**updateCallAnnotation**](CallsApi.md#updateCallAnnotation) | **PUT** /calls/{call_id}/annotations/ | Annotate highlights for a call
[**updateCallLock**](CallsApi.md#updateCallLock) | **PUT** /calls/{id}/lock/ | Locks or updates the annotation lock of a call


<a name="getAggregatedCalls"></a>
# **getAggregatedCalls**
> ResponseAggregatedCalls getAggregatedCalls(opts)

Returns aggregations for a group of calls.

Returns aggregations for call group selected by given criteria.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var opts = { 
  'sortBy': "size", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20, // Number | The maximum size of the page entries
  'callGroup': new BehavioralsignalsUapiClient.CallGroup() // CallGroup | Call group data
};
apiInstance.getAggregatedCalls(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sortBy** | **String**| Sorting option | [optional] [default to size]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]
 **callGroup** | [**CallGroup**](CallGroup.md)| Call group data | [optional] 

### Return type

[**ResponseAggregatedCalls**](ResponseAggregatedCalls.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getCalls"></a>
# **getCalls**
> ResponseCallsList getCalls(opts)

Returns a list of calls.

Returns a list of calls.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var opts = { 
  'agentId': "agentId_example", // String | Filters calls based on agent
  'campaignId': "campaignId_example", // String | Filters calls based on campaign
  'campaignLegacyId': "campaignLegacyId_example", // String | Filters calls based on legacy campaign
  'bookmarkcategoryId': 56, // Number | Filters calls based on a bookmark category
  'startDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Filters calls based on the start of a date range
  'endDate': new Date("2013-10-20T19:20:30+01:00"), // Date | Filters calls based on the end of a date range
  'sortBy': "id", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20 // Number | The maximum size of the page entries
};
apiInstance.getCalls(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **agentId** | **String**| Filters calls based on agent | [optional] 
 **campaignId** | **String**| Filters calls based on campaign | [optional] 
 **campaignLegacyId** | **String**| Filters calls based on legacy campaign | [optional] 
 **bookmarkcategoryId** | **Number**| Filters calls based on a bookmark category | [optional] 
 **startDate** | **Date**| Filters calls based on the start of a date range | [optional] 
 **endDate** | **Date**| Filters calls based on the end of a date range | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to id]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]

### Return type

[**ResponseCallsList**](ResponseCallsList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="getCallsCallId"></a>
# **getCallsCallId**
> CallDetails getCallsCallId(callId)

Returns details for call with id call_id.

Returns details for call with id call_id.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var callId = 56; // Number | The call id

apiInstance.getCallsCallId(callId).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callId** | **Number**| The call id | 

### Return type

[**CallDetails**](CallDetails.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json, text/csv

<a name="removeCallLock"></a>
# **removeCallLock**
> SuccessModel removeCallLock(id)

Removes the annotation lock of a call

Removes the annotation lock of a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var id = 56; // Number | The call id

apiInstance.removeCallLock(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The call id | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCallAnnotation"></a>
# **updateCallAnnotation**
> SuccessModel updateCallAnnotation(callId, opts)

Annotate highlights for a call

Annotate highlights for a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var callId = 56; // Number | The call id

var opts = { 
  'callAnnotation': new BehavioralsignalsUapiClient.RequestCallAnnotationModel() // RequestCallAnnotationModel | Annotations
};
apiInstance.updateCallAnnotation(callId, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callId** | **Number**| The call id | 
 **callAnnotation** | [**RequestCallAnnotationModel**](RequestCallAnnotationModel.md)| Annotations | [optional] 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateCallLock"></a>
# **updateCallLock**
> SuccessModel updateCallLock(id)

Locks or updates the annotation lock of a call

Locks or updates the annotation lock of a call

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.CallsApi();

var id = 56; // Number | The call id

apiInstance.updateCallLock(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The call id | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

