# BehavioralsignalsUapiClient.BookmarksApi

All URIs are relative to *https://uapi1.behavioralsignals.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addBookmark**](BookmarksApi.md#addBookmark) | **POST** /bookmarks/ | Assigns a specified call to a specific bookmark category
[**deleteBookmarkCategory**](BookmarksApi.md#deleteBookmarkCategory) | **DELETE** /bookmark-categories/{id}/ | Delete a Bookmark Category
[**getBookmarkCategories**](BookmarksApi.md#getBookmarkCategories) | **GET** /bookmark-categories/ | Returns a list of bookmark categories.
[**getBookmarkCategory**](BookmarksApi.md#getBookmarkCategory) | **GET** /bookmark-categories/{id}/ | Get the data of a single bookmark category
[**postBookmarkCategory**](BookmarksApi.md#postBookmarkCategory) | **POST** /bookmark-categories/ | Create a new Bookmark Category
[**removeBookmark**](BookmarksApi.md#removeBookmark) | **PUT** /bookmarks/ | Removes a specified call from a specific bookmark category
[**updateBookmarkCategory**](BookmarksApi.md#updateBookmarkCategory) | **PUT** /bookmark-categories/{id}/ | Update a Bookmark Category


<a name="addBookmark"></a>
# **addBookmark**
> SuccessModel addBookmark(bookmark)

Assigns a specified call to a specific bookmark category

Assigns a specified call to a specific bookmark category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var bookmark = new BehavioralsignalsUapiClient.BookmarkModel(); // BookmarkModel | A bookmark object

apiInstance.addBookmark(bookmark).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookmark** | [**BookmarkModel**](BookmarkModel.md)| A bookmark object | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteBookmarkCategory"></a>
# **deleteBookmarkCategory**
> SuccessModel deleteBookmarkCategory(id)

Delete a Bookmark Category

Deletes a bookmark category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var id = 56; // Number | The id of the bookmark category

apiInstance.deleteBookmarkCategory(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The id of the bookmark category | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBookmarkCategories"></a>
# **getBookmarkCategories**
> ResponseBookmarkCategoriesList getBookmarkCategories(opts)

Returns a list of bookmark categories.

Returns a list of bookmark categories.

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var opts = { 
  'callId': 56, // Number | Filters bookmark categories based on call id
  'sortBy': "id", // String | Sorting option
  'page': 1, // Number | Page number
  'limit': 20 // Number | The maximum size of the page entries
};
apiInstance.getBookmarkCategories(opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **callId** | **Number**| Filters bookmark categories based on call id | [optional] 
 **sortBy** | **String**| Sorting option | [optional] [default to id]
 **page** | **Number**| Page number | [optional] [default to 1]
 **limit** | **Number**| The maximum size of the page entries | [optional] [default to 20]

### Return type

[**ResponseBookmarkCategoriesList**](ResponseBookmarkCategoriesList.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getBookmarkCategory"></a>
# **getBookmarkCategory**
> ResponseAggregatedBookmarkCategory getBookmarkCategory(id)

Get the data of a single bookmark category

Get the data of a single bookmark category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var id = 56; // Number | The unique identifier of the bookmark category

apiInstance.getBookmarkCategory(id).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| The unique identifier of the bookmark category | 

### Return type

[**ResponseAggregatedBookmarkCategory**](ResponseAggregatedBookmarkCategory.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="postBookmarkCategory"></a>
# **postBookmarkCategory**
> CreationModel postBookmarkCategory(bookmarkCategory)

Create a new Bookmark Category

Create a new Bookmark Category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var bookmarkCategory = new BehavioralsignalsUapiClient.BookmarkCategoryModel(); // BookmarkCategoryModel | A new bookmark category object

apiInstance.postBookmarkCategory(bookmarkCategory).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookmarkCategory** | [**BookmarkCategoryModel**](BookmarkCategoryModel.md)| A new bookmark category object | 

### Return type

[**CreationModel**](CreationModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeBookmark"></a>
# **removeBookmark**
> SuccessModel removeBookmark(bookmark)

Removes a specified call from a specific bookmark category

Removes a specified call from a specific bookmark category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var bookmark = new BehavioralsignalsUapiClient.BookmarkModel(); // BookmarkModel | A bookmark object

apiInstance.removeBookmark(bookmark).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **bookmark** | [**BookmarkModel**](BookmarkModel.md)| A bookmark object | 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateBookmarkCategory"></a>
# **updateBookmarkCategory**
> SuccessModel updateBookmarkCategory(id, opts)

Update a Bookmark Category

Update a Bookmark Category

### Example
```javascript
var BehavioralsignalsUapiClient = require('@behavioralsignals/behavioralsignals-uapi-client');
var defaultClient = BehavioralsignalsUapiClient.ApiClient.instance;

// Configure OAuth2 access token for authorization: OAuth2
var OAuth2 = defaultClient.authentications['OAuth2'];
OAuth2.accessToken = 'YOUR ACCESS TOKEN';

var apiInstance = new BehavioralsignalsUapiClient.BookmarksApi();

var id = 56; // Number | id

var opts = { 
  'bookmarkCategory': new BehavioralsignalsUapiClient.BookmarkCategoryModel() // BookmarkCategoryModel | Bookmark category
};
apiInstance.updateBookmarkCategory(id, opts).then(function(data) {
  console.log('API called successfully. Returned data: ' + data);
}, function(error) {
  console.error(error);
});

```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| id | 
 **bookmarkCategory** | [**BookmarkCategoryModel**](BookmarkCategoryModel.md)| Bookmark category | [optional] 

### Return type

[**SuccessModel**](SuccessModel.md)

### Authorization

[OAuth2](../README.md#OAuth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

