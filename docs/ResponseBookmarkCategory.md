# BehavioralsignalsUapiClient.ResponseBookmarkCategory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Number** | The id of the bookmark category | [optional] 
**title** | **String** | The title of the bookmark category | [optional] [default to &#39;Untitled Folder&#39;]
**desc** | **String** | A description of the bookmark category | [optional] 
**callNum** | **Number** | The number of calls belonging on that bookmark category | [optional] 
**createdAt** | **String** | The datetime of the creation of the bookmark category | [optional] 
**modifiedAt** | **String** | The datetime of the modification of the bookmark category | [optional] 


