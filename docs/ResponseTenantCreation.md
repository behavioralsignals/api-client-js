# BehavioralsignalsUapiClient.ResponseTenantCreation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** | Unique tenant name on backend | [optional] 
**tenantId** | **String** | Unique tenant identifier on backend | [optional] 
**cid** | **Number** | Unique tenant identifier on service | [optional] 
**token** | **String** | Tenant token on service | [optional] 


