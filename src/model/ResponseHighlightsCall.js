/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ResponseHighlightsCallSpeakers', 'model/ResponseHighlightsFrame'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./ResponseHighlightsCallSpeakers'), require('./ResponseHighlightsFrame'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.ResponseHighlightsCall = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.ResponseHighlightsCallSpeakers, root.BehavioralsignalsUapiClient.ResponseHighlightsFrame);
  }
}(this, function(ApiClient, ResponseHighlightsCallSpeakers, ResponseHighlightsFrame) {
  'use strict';




  /**
   * The ResponseHighlightsCall model module.
   * @module model/ResponseHighlightsCall
   * @version 0.2.18
   */

  /**
   * Constructs a new <code>ResponseHighlightsCall</code>.
   * @alias module:model/ResponseHighlightsCall
   * @class
   * @param speakers {module:model/ResponseHighlightsCallSpeakers} 
   * @param datetime {Date} 
   * @param playbackUrl {String} 
   * @param duration {String} 
   * @param frames {Array.<module:model/ResponseHighlightsFrame>} 
   */
  var exports = function(speakers, datetime, playbackUrl, duration, frames) {
    var _this = this;

    _this['speakers'] = speakers;
    _this['datetime'] = datetime;

    _this['playback_url'] = playbackUrl;

    _this['duration'] = duration;
    _this['frames'] = frames;
  };

  /**
   * Constructs a <code>ResponseHighlightsCall</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ResponseHighlightsCall} obj Optional instance to populate.
   * @return {module:model/ResponseHighlightsCall} The populated <code>ResponseHighlightsCall</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('speakers')) {
        obj['speakers'] = ResponseHighlightsCallSpeakers.constructFromObject(data['speakers']);
      }
      if (data.hasOwnProperty('datetime')) {
        obj['datetime'] = ApiClient.convertToType(data['datetime'], 'Date');
      }
      if (data.hasOwnProperty('campaign_titles')) {
        obj['campaign_titles'] = ApiClient.convertToType(data['campaign_titles'], ['String']);
      }
      if (data.hasOwnProperty('playback_url')) {
        obj['playback_url'] = ApiClient.convertToType(data['playback_url'], 'String');
      }
      if (data.hasOwnProperty('calldirection')) {
        obj['calldirection'] = ApiClient.convertToType(data['calldirection'], 'Number');
      }
      if (data.hasOwnProperty('duration')) {
        obj['duration'] = ApiClient.convertToType(data['duration'], 'String');
      }
      if (data.hasOwnProperty('frames')) {
        obj['frames'] = ApiClient.convertToType(data['frames'], [ResponseHighlightsFrame]);
      }
    }
    return obj;
  }

  /**
   * @member {module:model/ResponseHighlightsCallSpeakers} speakers
   */
  exports.prototype['speakers'] = undefined;
  /**
   * @member {Date} datetime
   */
  exports.prototype['datetime'] = undefined;
  /**
   * @member {Array.<String>} campaign_titles
   */
  exports.prototype['campaign_titles'] = undefined;
  /**
   * @member {String} playback_url
   */
  exports.prototype['playback_url'] = undefined;
  /**
   * @member {Number} calldirection
   */
  exports.prototype['calldirection'] = undefined;
  /**
   * @member {String} duration
   */
  exports.prototype['duration'] = undefined;
  /**
   * @member {Array.<module:model/ResponseHighlightsFrame>} frames
   */
  exports.prototype['frames'] = undefined;



  return exports;
}));


