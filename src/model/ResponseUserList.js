/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/PaginationModel', 'model/UserDetails'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./PaginationModel'), require('./UserDetails'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.ResponseUserList = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.PaginationModel, root.BehavioralsignalsUapiClient.UserDetails);
  }
}(this, function(ApiClient, PaginationModel, UserDetails) {
  'use strict';




  /**
   * The ResponseUserList model module.
   * @module model/ResponseUserList
   * @version 0.2.18
   */

  /**
   * Constructs a new <code>ResponseUserList</code>.
   * @alias module:model/ResponseUserList
   * @class
   * @implements module:model/PaginationModel
   */
  var exports = function() {
    var _this = this;

    PaginationModel.call(_this);

  };

  /**
   * Constructs a <code>ResponseUserList</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ResponseUserList} obj Optional instance to populate.
   * @return {module:model/ResponseUserList} The populated <code>ResponseUserList</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      PaginationModel.constructFromObject(data, obj);
      if (data.hasOwnProperty('results')) {
        obj['results'] = ApiClient.convertToType(data['results'], [UserDetails]);
      }
    }
    return obj;
  }

  /**
   * @member {Array.<module:model/UserDetails>} results
   */
  exports.prototype['results'] = undefined;

  // Implement PaginationModel interface:
  /**
   * @member {String} next
   */
exports.prototype['next'] = undefined;

  /**
   * @member {String} previous
   */
exports.prototype['previous'] = undefined;

  /**
   * @member {Number} count
   */
exports.prototype['count'] = undefined;



  return exports;
}));


