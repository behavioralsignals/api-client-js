/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/RequestAnnotationComments', 'model/RequestAnnotationsModel'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./RequestAnnotationComments'), require('./RequestAnnotationsModel'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.RequestCallAnnotationModelCallAnnotation = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.RequestAnnotationComments, root.BehavioralsignalsUapiClient.RequestAnnotationsModel);
  }
}(this, function(ApiClient, RequestAnnotationComments, RequestAnnotationsModel) {
  'use strict';




  /**
   * The RequestCallAnnotationModelCallAnnotation model module.
   * @module model/RequestCallAnnotationModelCallAnnotation
   * @version 0.2.18
   */

  /**
   * Constructs a new <code>RequestCallAnnotationModelCallAnnotation</code>.
   * @alias module:model/RequestCallAnnotationModelCallAnnotation
   * @class
   * @param annotations {module:model/RequestAnnotationsModel} 
   * @param comments {module:model/RequestAnnotationComments} 
   */
  var exports = function(annotations, comments) {
    var _this = this;

    _this['annotations'] = annotations;
    _this['comments'] = comments;

  };

  /**
   * Constructs a <code>RequestCallAnnotationModelCallAnnotation</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/RequestCallAnnotationModelCallAnnotation} obj Optional instance to populate.
   * @return {module:model/RequestCallAnnotationModelCallAnnotation} The populated <code>RequestCallAnnotationModelCallAnnotation</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('annotations')) {
        obj['annotations'] = RequestAnnotationsModel.constructFromObject(data['annotations']);
      }
      if (data.hasOwnProperty('comments')) {
        obj['comments'] = RequestAnnotationComments.constructFromObject(data['comments']);
      }
      if (data.hasOwnProperty('call_retrieve_timestamp')) {
        obj['call_retrieve_timestamp'] = ApiClient.convertToType(data['call_retrieve_timestamp'], 'String');
      }
    }
    return obj;
  }

  /**
   * @member {module:model/RequestAnnotationsModel} annotations
   */
  exports.prototype['annotations'] = undefined;
  /**
   * @member {module:model/RequestAnnotationComments} comments
   */
  exports.prototype['comments'] = undefined;
  /**
   * Timestamp that the call was retrieved by the client
   * @member {String} call_retrieve_timestamp
   */
  exports.prototype['call_retrieve_timestamp'] = undefined;



  return exports;
}));


