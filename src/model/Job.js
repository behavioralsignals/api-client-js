/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/JobJobStatus', 'model/JobMediaMetadata'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('./JobJobStatus'), require('./JobMediaMetadata'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.Job = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.JobJobStatus, root.BehavioralsignalsUapiClient.JobMediaMetadata);
  }
}(this, function(ApiClient, JobJobStatus, JobMediaMetadata) {
  'use strict';




  /**
   * The Job model module.
   * @module model/Job
   * @version 0.2.18
   */

  /**
   * Constructs a new <code>Job</code>.
   * A Job object used for representing featured jobs
   * @alias module:model/Job
   * @class
   */
  var exports = function() {
    var _this = this;









  };

  /**
   * Constructs a <code>Job</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/Job} obj Optional instance to populate.
   * @return {module:model/Job} The populated <code>Job</code> instance.
   */
  exports.constructFromObject = function(data, obj) {
    if (data) {
      obj = obj || new exports();

      if (data.hasOwnProperty('job_id')) {
        obj['job_id'] = ApiClient.convertToType(data['job_id'], 'Number');
      }
      if (data.hasOwnProperty('filename')) {
        obj['filename'] = ApiClient.convertToType(data['filename'], 'String');
      }
      if (data.hasOwnProperty('batch_id')) {
        obj['batch_id'] = ApiClient.convertToType(data['batch_id'], 'String');
      }
      if (data.hasOwnProperty('uploaded')) {
        obj['uploaded'] = ApiClient.convertToType(data['uploaded'], 'String');
      }
      if (data.hasOwnProperty('storedata')) {
        obj['storedata'] = ApiClient.convertToType(data['storedata'], 'Number');
      }
      if (data.hasOwnProperty('uri')) {
        obj['uri'] = ApiClient.convertToType(data['uri'], 'String');
      }
      if (data.hasOwnProperty('media_metadata')) {
        obj['media_metadata'] = JobMediaMetadata.constructFromObject(data['media_metadata']);
      }
      if (data.hasOwnProperty('job_status')) {
        obj['job_status'] = JobJobStatus.constructFromObject(data['job_status']);
      }
    }
    return obj;
  }

  /**
   * Unique identifier for the job
   * @member {Number} job_id
   */
  exports.prototype['job_id'] = undefined;
  /**
   * Name of the call recording
   * @member {String} filename
   */
  exports.prototype['filename'] = undefined;
  /**
   * Unique identifier or a batch of calls
   * @member {String} batch_id
   */
  exports.prototype['batch_id'] = undefined;
  /**
   * Unique identifier or a batch of calls
   * @member {String} uploaded
   */
  exports.prototype['uploaded'] = undefined;
  /**
   * Unique identifier for the job
   * @member {Number} storedata
   */
  exports.prototype['storedata'] = undefined;
  /**
   * Unique identifier or a batch of calls
   * @member {String} uri
   */
  exports.prototype['uri'] = undefined;
  /**
   * @member {module:model/JobMediaMetadata} media_metadata
   */
  exports.prototype['media_metadata'] = undefined;
  /**
   * @member {module:model/JobJobStatus} job_status
   */
  exports.prototype['job_status'] = undefined;



  return exports;
}));


