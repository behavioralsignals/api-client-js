/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/AccountSubscription', 'model/AnnotationCommentsHighlight', 'model/AnnotationCommentsHighlights', 'model/AnnotationHighlight', 'model/AnnotationSpeaker', 'model/Behavior', 'model/BehaviorInner', 'model/BehaviorInnerValue', 'model/BookmarkCategoryModel', 'model/BookmarkModel', 'model/CalculationModel', 'model/Call', 'model/CallDetailsBehaviors', 'model/CallDetailsTrends', 'model/CallGroup', 'model/Campaign', 'model/CampaignRequest', 'model/Company', 'model/CreateTenant', 'model/CreationModel', 'model/Employee', 'model/EmployeesErrorModel', 'model/ErrorModel', 'model/IndicatorType', 'model/Job', 'model/JobCreationModel', 'model/JobErrorModel', 'model/JobJobStatus', 'model/JobMediaMetadata', 'model/JobsErrorModel', 'model/LegacyCampaign', 'model/PaginationModel', 'model/RequestAnnotationComments', 'model/RequestAnnotationsModel', 'model/RequestAnnotationsModelSpeakers', 'model/RequestCallAnnotationModel', 'model/RequestCallAnnotationModelCallAnnotation', 'model/ResponseAgents', 'model/ResponseAggregations', 'model/ResponseBookmarkCategory', 'model/ResponseCompanies', 'model/ResponseHighlightNote', 'model/ResponseHighlightsBehavior', 'model/ResponseHighlightsCall', 'model/ResponseHighlightsCallSpeakers', 'model/ResponseHighlightsFrame', 'model/ResponseHighlightsHighlight', 'model/ResponseHighlightsIndicator', 'model/ResponseHighlightsSpeaker', 'model/ResponseHighlightsSpeakerTalkTime', 'model/ResponseStreamAudio', 'model/ResponseTenantCreation', 'model/SuccessModel', 'model/UserCreation', 'model/UserCreationDetails', 'model/UserDetails', 'model/UserDetailsDetails', 'model/UserNotificationDetails', 'model/CallDetails', 'model/ResponseAggregatedAgent', 'model/ResponseAggregatedAgents', 'model/ResponseAggregatedBookmarkCategory', 'model/ResponseAggregatedCalls', 'model/ResponseAggregatedCampaign', 'model/ResponseAggregatedCampaigns', 'model/ResponseAggregatedLegacyCampaign', 'model/ResponseAggregatedLegacyCampaigns', 'model/ResponseBookmarkCategoriesList', 'model/ResponseCallsList', 'model/ResponseCampaignAggregations', 'model/ResponseCampaigns', 'model/ResponseEmployeesList', 'model/ResponseHighlightsAgent', 'model/ResponseHighlightsCustomer', 'model/ResponseJobList', 'model/ResponseLegacyCampaigns', 'model/ResponseUserList', 'model/ScoredIndicatorType', 'api/AccountsApi', 'api/AgentsApi', 'api/AggregationsApi', 'api/AnnotationsApi', 'api/BookmarksApi', 'api/CallsApi', 'api/CampaignsApi', 'api/CampaignslegacyApi', 'api/CompaniesApi', 'api/ConfirmationApi', 'api/EmployeesApi', 'api/JobsApi', 'api/MeApi', 'api/ProcessesApi', 'api/ProfileApi', 'api/TenantApi', 'api/UsersApi'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('./ApiClient'), require('./model/AccountSubscription'), require('./model/AnnotationCommentsHighlight'), require('./model/AnnotationCommentsHighlights'), require('./model/AnnotationHighlight'), require('./model/AnnotationSpeaker'), require('./model/Behavior'), require('./model/BehaviorInner'), require('./model/BehaviorInnerValue'), require('./model/BookmarkCategoryModel'), require('./model/BookmarkModel'), require('./model/CalculationModel'), require('./model/Call'), require('./model/CallDetailsBehaviors'), require('./model/CallDetailsTrends'), require('./model/CallGroup'), require('./model/Campaign'), require('./model/CampaignRequest'), require('./model/Company'), require('./model/CreateTenant'), require('./model/CreationModel'), require('./model/Employee'), require('./model/EmployeesErrorModel'), require('./model/ErrorModel'), require('./model/IndicatorType'), require('./model/Job'), require('./model/JobCreationModel'), require('./model/JobErrorModel'), require('./model/JobJobStatus'), require('./model/JobMediaMetadata'), require('./model/JobsErrorModel'), require('./model/LegacyCampaign'), require('./model/PaginationModel'), require('./model/RequestAnnotationComments'), require('./model/RequestAnnotationsModel'), require('./model/RequestAnnotationsModelSpeakers'), require('./model/RequestCallAnnotationModel'), require('./model/RequestCallAnnotationModelCallAnnotation'), require('./model/ResponseAgents'), require('./model/ResponseAggregations'), require('./model/ResponseBookmarkCategory'), require('./model/ResponseCompanies'), require('./model/ResponseHighlightNote'), require('./model/ResponseHighlightsBehavior'), require('./model/ResponseHighlightsCall'), require('./model/ResponseHighlightsCallSpeakers'), require('./model/ResponseHighlightsFrame'), require('./model/ResponseHighlightsHighlight'), require('./model/ResponseHighlightsIndicator'), require('./model/ResponseHighlightsSpeaker'), require('./model/ResponseHighlightsSpeakerTalkTime'), require('./model/ResponseStreamAudio'), require('./model/ResponseTenantCreation'), require('./model/SuccessModel'), require('./model/UserCreation'), require('./model/UserCreationDetails'), require('./model/UserDetails'), require('./model/UserDetailsDetails'), require('./model/UserNotificationDetails'), require('./model/CallDetails'), require('./model/ResponseAggregatedAgent'), require('./model/ResponseAggregatedAgents'), require('./model/ResponseAggregatedBookmarkCategory'), require('./model/ResponseAggregatedCalls'), require('./model/ResponseAggregatedCampaign'), require('./model/ResponseAggregatedCampaigns'), require('./model/ResponseAggregatedLegacyCampaign'), require('./model/ResponseAggregatedLegacyCampaigns'), require('./model/ResponseBookmarkCategoriesList'), require('./model/ResponseCallsList'), require('./model/ResponseCampaignAggregations'), require('./model/ResponseCampaigns'), require('./model/ResponseEmployeesList'), require('./model/ResponseHighlightsAgent'), require('./model/ResponseHighlightsCustomer'), require('./model/ResponseJobList'), require('./model/ResponseLegacyCampaigns'), require('./model/ResponseUserList'), require('./model/ScoredIndicatorType'), require('./api/AccountsApi'), require('./api/AgentsApi'), require('./api/AggregationsApi'), require('./api/AnnotationsApi'), require('./api/BookmarksApi'), require('./api/CallsApi'), require('./api/CampaignsApi'), require('./api/CampaignslegacyApi'), require('./api/CompaniesApi'), require('./api/ConfirmationApi'), require('./api/EmployeesApi'), require('./api/JobsApi'), require('./api/MeApi'), require('./api/ProcessesApi'), require('./api/ProfileApi'), require('./api/TenantApi'), require('./api/UsersApi'));
  }
}(function(ApiClient, AccountSubscription, AnnotationCommentsHighlight, AnnotationCommentsHighlights, AnnotationHighlight, AnnotationSpeaker, Behavior, BehaviorInner, BehaviorInnerValue, BookmarkCategoryModel, BookmarkModel, CalculationModel, Call, CallDetailsBehaviors, CallDetailsTrends, CallGroup, Campaign, CampaignRequest, Company, CreateTenant, CreationModel, Employee, EmployeesErrorModel, ErrorModel, IndicatorType, Job, JobCreationModel, JobErrorModel, JobJobStatus, JobMediaMetadata, JobsErrorModel, LegacyCampaign, PaginationModel, RequestAnnotationComments, RequestAnnotationsModel, RequestAnnotationsModelSpeakers, RequestCallAnnotationModel, RequestCallAnnotationModelCallAnnotation, ResponseAgents, ResponseAggregations, ResponseBookmarkCategory, ResponseCompanies, ResponseHighlightNote, ResponseHighlightsBehavior, ResponseHighlightsCall, ResponseHighlightsCallSpeakers, ResponseHighlightsFrame, ResponseHighlightsHighlight, ResponseHighlightsIndicator, ResponseHighlightsSpeaker, ResponseHighlightsSpeakerTalkTime, ResponseStreamAudio, ResponseTenantCreation, SuccessModel, UserCreation, UserCreationDetails, UserDetails, UserDetailsDetails, UserNotificationDetails, CallDetails, ResponseAggregatedAgent, ResponseAggregatedAgents, ResponseAggregatedBookmarkCategory, ResponseAggregatedCalls, ResponseAggregatedCampaign, ResponseAggregatedCampaigns, ResponseAggregatedLegacyCampaign, ResponseAggregatedLegacyCampaigns, ResponseBookmarkCategoriesList, ResponseCallsList, ResponseCampaignAggregations, ResponseCampaigns, ResponseEmployeesList, ResponseHighlightsAgent, ResponseHighlightsCustomer, ResponseJobList, ResponseLegacyCampaigns, ResponseUserList, ScoredIndicatorType, AccountsApi, AgentsApi, AggregationsApi, AnnotationsApi, BookmarksApi, CallsApi, CampaignsApi, CampaignslegacyApi, CompaniesApi, ConfirmationApi, EmployeesApi, JobsApi, MeApi, ProcessesApi, ProfileApi, TenantApi, UsersApi) {
  'use strict';

  /**
   * Behavioral_Signal_Technologies_API_enables_third_party_applications_or_processes_to_process_collected_data_in_the_cloud_and_provides_emotional_and_demographic_insights_for_consumption_.<br>
   * The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
   * <p>
   * An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
   * <pre>
   * var BehavioralsignalsUapiClient = require('index'); // See note below*.
   * var xxxSvc = new BehavioralsignalsUapiClient.XxxApi(); // Allocate the API class we're going to use.
   * var yyyModel = new BehavioralsignalsUapiClient.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
   * and put the application logic within the callback function.</em>
   * </p>
   * <p>
   * A non-AMD browser application (discouraged) might do something like this:
   * <pre>
   * var xxxSvc = new BehavioralsignalsUapiClient.XxxApi(); // Allocate the API class we're going to use.
   * var yyy = new BehavioralsignalsUapiClient.Yyy(); // Construct a model instance.
   * yyyModel.someProperty = 'someValue';
   * ...
   * var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
   * ...
   * </pre>
   * </p>
   * @module index
   * @version 0.2.18
   */
  var exports = {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient: ApiClient,
    /**
     * The AccountSubscription model constructor.
     * @property {module:model/AccountSubscription}
     */
    AccountSubscription: AccountSubscription,
    /**
     * The AnnotationCommentsHighlight model constructor.
     * @property {module:model/AnnotationCommentsHighlight}
     */
    AnnotationCommentsHighlight: AnnotationCommentsHighlight,
    /**
     * The AnnotationCommentsHighlights model constructor.
     * @property {module:model/AnnotationCommentsHighlights}
     */
    AnnotationCommentsHighlights: AnnotationCommentsHighlights,
    /**
     * The AnnotationHighlight model constructor.
     * @property {module:model/AnnotationHighlight}
     */
    AnnotationHighlight: AnnotationHighlight,
    /**
     * The AnnotationSpeaker model constructor.
     * @property {module:model/AnnotationSpeaker}
     */
    AnnotationSpeaker: AnnotationSpeaker,
    /**
     * The Behavior model constructor.
     * @property {module:model/Behavior}
     */
    Behavior: Behavior,
    /**
     * The BehaviorInner model constructor.
     * @property {module:model/BehaviorInner}
     */
    BehaviorInner: BehaviorInner,
    /**
     * The BehaviorInnerValue model constructor.
     * @property {module:model/BehaviorInnerValue}
     */
    BehaviorInnerValue: BehaviorInnerValue,
    /**
     * The BookmarkCategoryModel model constructor.
     * @property {module:model/BookmarkCategoryModel}
     */
    BookmarkCategoryModel: BookmarkCategoryModel,
    /**
     * The BookmarkModel model constructor.
     * @property {module:model/BookmarkModel}
     */
    BookmarkModel: BookmarkModel,
    /**
     * The CalculationModel model constructor.
     * @property {module:model/CalculationModel}
     */
    CalculationModel: CalculationModel,
    /**
     * The Call model constructor.
     * @property {module:model/Call}
     */
    Call: Call,
    /**
     * The CallDetailsBehaviors model constructor.
     * @property {module:model/CallDetailsBehaviors}
     */
    CallDetailsBehaviors: CallDetailsBehaviors,
    /**
     * The CallDetailsTrends model constructor.
     * @property {module:model/CallDetailsTrends}
     */
    CallDetailsTrends: CallDetailsTrends,
    /**
     * The CallGroup model constructor.
     * @property {module:model/CallGroup}
     */
    CallGroup: CallGroup,
    /**
     * The Campaign model constructor.
     * @property {module:model/Campaign}
     */
    Campaign: Campaign,
    /**
     * The CampaignRequest model constructor.
     * @property {module:model/CampaignRequest}
     */
    CampaignRequest: CampaignRequest,
    /**
     * The Company model constructor.
     * @property {module:model/Company}
     */
    Company: Company,
    /**
     * The CreateTenant model constructor.
     * @property {module:model/CreateTenant}
     */
    CreateTenant: CreateTenant,
    /**
     * The CreationModel model constructor.
     * @property {module:model/CreationModel}
     */
    CreationModel: CreationModel,
    /**
     * The Employee model constructor.
     * @property {module:model/Employee}
     */
    Employee: Employee,
    /**
     * The EmployeesErrorModel model constructor.
     * @property {module:model/EmployeesErrorModel}
     */
    EmployeesErrorModel: EmployeesErrorModel,
    /**
     * The ErrorModel model constructor.
     * @property {module:model/ErrorModel}
     */
    ErrorModel: ErrorModel,
    /**
     * The IndicatorType model constructor.
     * @property {module:model/IndicatorType}
     */
    IndicatorType: IndicatorType,
    /**
     * The Job model constructor.
     * @property {module:model/Job}
     */
    Job: Job,
    /**
     * The JobCreationModel model constructor.
     * @property {module:model/JobCreationModel}
     */
    JobCreationModel: JobCreationModel,
    /**
     * The JobErrorModel model constructor.
     * @property {module:model/JobErrorModel}
     */
    JobErrorModel: JobErrorModel,
    /**
     * The JobJobStatus model constructor.
     * @property {module:model/JobJobStatus}
     */
    JobJobStatus: JobJobStatus,
    /**
     * The JobMediaMetadata model constructor.
     * @property {module:model/JobMediaMetadata}
     */
    JobMediaMetadata: JobMediaMetadata,
    /**
     * The JobsErrorModel model constructor.
     * @property {module:model/JobsErrorModel}
     */
    JobsErrorModel: JobsErrorModel,
    /**
     * The LegacyCampaign model constructor.
     * @property {module:model/LegacyCampaign}
     */
    LegacyCampaign: LegacyCampaign,
    /**
     * The PaginationModel model constructor.
     * @property {module:model/PaginationModel}
     */
    PaginationModel: PaginationModel,
    /**
     * The RequestAnnotationComments model constructor.
     * @property {module:model/RequestAnnotationComments}
     */
    RequestAnnotationComments: RequestAnnotationComments,
    /**
     * The RequestAnnotationsModel model constructor.
     * @property {module:model/RequestAnnotationsModel}
     */
    RequestAnnotationsModel: RequestAnnotationsModel,
    /**
     * The RequestAnnotationsModelSpeakers model constructor.
     * @property {module:model/RequestAnnotationsModelSpeakers}
     */
    RequestAnnotationsModelSpeakers: RequestAnnotationsModelSpeakers,
    /**
     * The RequestCallAnnotationModel model constructor.
     * @property {module:model/RequestCallAnnotationModel}
     */
    RequestCallAnnotationModel: RequestCallAnnotationModel,
    /**
     * The RequestCallAnnotationModelCallAnnotation model constructor.
     * @property {module:model/RequestCallAnnotationModelCallAnnotation}
     */
    RequestCallAnnotationModelCallAnnotation: RequestCallAnnotationModelCallAnnotation,
    /**
     * The ResponseAgents model constructor.
     * @property {module:model/ResponseAgents}
     */
    ResponseAgents: ResponseAgents,
    /**
     * The ResponseAggregations model constructor.
     * @property {module:model/ResponseAggregations}
     */
    ResponseAggregations: ResponseAggregations,
    /**
     * The ResponseBookmarkCategory model constructor.
     * @property {module:model/ResponseBookmarkCategory}
     */
    ResponseBookmarkCategory: ResponseBookmarkCategory,
    /**
     * The ResponseCompanies model constructor.
     * @property {module:model/ResponseCompanies}
     */
    ResponseCompanies: ResponseCompanies,
    /**
     * The ResponseHighlightNote model constructor.
     * @property {module:model/ResponseHighlightNote}
     */
    ResponseHighlightNote: ResponseHighlightNote,
    /**
     * The ResponseHighlightsBehavior model constructor.
     * @property {module:model/ResponseHighlightsBehavior}
     */
    ResponseHighlightsBehavior: ResponseHighlightsBehavior,
    /**
     * The ResponseHighlightsCall model constructor.
     * @property {module:model/ResponseHighlightsCall}
     */
    ResponseHighlightsCall: ResponseHighlightsCall,
    /**
     * The ResponseHighlightsCallSpeakers model constructor.
     * @property {module:model/ResponseHighlightsCallSpeakers}
     */
    ResponseHighlightsCallSpeakers: ResponseHighlightsCallSpeakers,
    /**
     * The ResponseHighlightsFrame model constructor.
     * @property {module:model/ResponseHighlightsFrame}
     */
    ResponseHighlightsFrame: ResponseHighlightsFrame,
    /**
     * The ResponseHighlightsHighlight model constructor.
     * @property {module:model/ResponseHighlightsHighlight}
     */
    ResponseHighlightsHighlight: ResponseHighlightsHighlight,
    /**
     * The ResponseHighlightsIndicator model constructor.
     * @property {module:model/ResponseHighlightsIndicator}
     */
    ResponseHighlightsIndicator: ResponseHighlightsIndicator,
    /**
     * The ResponseHighlightsSpeaker model constructor.
     * @property {module:model/ResponseHighlightsSpeaker}
     */
    ResponseHighlightsSpeaker: ResponseHighlightsSpeaker,
    /**
     * The ResponseHighlightsSpeakerTalkTime model constructor.
     * @property {module:model/ResponseHighlightsSpeakerTalkTime}
     */
    ResponseHighlightsSpeakerTalkTime: ResponseHighlightsSpeakerTalkTime,
    /**
     * The ResponseStreamAudio model constructor.
     * @property {module:model/ResponseStreamAudio}
     */
    ResponseStreamAudio: ResponseStreamAudio,
    /**
     * The ResponseTenantCreation model constructor.
     * @property {module:model/ResponseTenantCreation}
     */
    ResponseTenantCreation: ResponseTenantCreation,
    /**
     * The SuccessModel model constructor.
     * @property {module:model/SuccessModel}
     */
    SuccessModel: SuccessModel,
    /**
     * The UserCreation model constructor.
     * @property {module:model/UserCreation}
     */
    UserCreation: UserCreation,
    /**
     * The UserCreationDetails model constructor.
     * @property {module:model/UserCreationDetails}
     */
    UserCreationDetails: UserCreationDetails,
    /**
     * The UserDetails model constructor.
     * @property {module:model/UserDetails}
     */
    UserDetails: UserDetails,
    /**
     * The UserDetailsDetails model constructor.
     * @property {module:model/UserDetailsDetails}
     */
    UserDetailsDetails: UserDetailsDetails,
    /**
     * The UserNotificationDetails model constructor.
     * @property {module:model/UserNotificationDetails}
     */
    UserNotificationDetails: UserNotificationDetails,
    /**
     * The CallDetails model constructor.
     * @property {module:model/CallDetails}
     */
    CallDetails: CallDetails,
    /**
     * The ResponseAggregatedAgent model constructor.
     * @property {module:model/ResponseAggregatedAgent}
     */
    ResponseAggregatedAgent: ResponseAggregatedAgent,
    /**
     * The ResponseAggregatedAgents model constructor.
     * @property {module:model/ResponseAggregatedAgents}
     */
    ResponseAggregatedAgents: ResponseAggregatedAgents,
    /**
     * The ResponseAggregatedBookmarkCategory model constructor.
     * @property {module:model/ResponseAggregatedBookmarkCategory}
     */
    ResponseAggregatedBookmarkCategory: ResponseAggregatedBookmarkCategory,
    /**
     * The ResponseAggregatedCalls model constructor.
     * @property {module:model/ResponseAggregatedCalls}
     */
    ResponseAggregatedCalls: ResponseAggregatedCalls,
    /**
     * The ResponseAggregatedCampaign model constructor.
     * @property {module:model/ResponseAggregatedCampaign}
     */
    ResponseAggregatedCampaign: ResponseAggregatedCampaign,
    /**
     * The ResponseAggregatedCampaigns model constructor.
     * @property {module:model/ResponseAggregatedCampaigns}
     */
    ResponseAggregatedCampaigns: ResponseAggregatedCampaigns,
    /**
     * The ResponseAggregatedLegacyCampaign model constructor.
     * @property {module:model/ResponseAggregatedLegacyCampaign}
     */
    ResponseAggregatedLegacyCampaign: ResponseAggregatedLegacyCampaign,
    /**
     * The ResponseAggregatedLegacyCampaigns model constructor.
     * @property {module:model/ResponseAggregatedLegacyCampaigns}
     */
    ResponseAggregatedLegacyCampaigns: ResponseAggregatedLegacyCampaigns,
    /**
     * The ResponseBookmarkCategoriesList model constructor.
     * @property {module:model/ResponseBookmarkCategoriesList}
     */
    ResponseBookmarkCategoriesList: ResponseBookmarkCategoriesList,
    /**
     * The ResponseCallsList model constructor.
     * @property {module:model/ResponseCallsList}
     */
    ResponseCallsList: ResponseCallsList,
    /**
     * The ResponseCampaignAggregations model constructor.
     * @property {module:model/ResponseCampaignAggregations}
     */
    ResponseCampaignAggregations: ResponseCampaignAggregations,
    /**
     * The ResponseCampaigns model constructor.
     * @property {module:model/ResponseCampaigns}
     */
    ResponseCampaigns: ResponseCampaigns,
    /**
     * The ResponseEmployeesList model constructor.
     * @property {module:model/ResponseEmployeesList}
     */
    ResponseEmployeesList: ResponseEmployeesList,
    /**
     * The ResponseHighlightsAgent model constructor.
     * @property {module:model/ResponseHighlightsAgent}
     */
    ResponseHighlightsAgent: ResponseHighlightsAgent,
    /**
     * The ResponseHighlightsCustomer model constructor.
     * @property {module:model/ResponseHighlightsCustomer}
     */
    ResponseHighlightsCustomer: ResponseHighlightsCustomer,
    /**
     * The ResponseJobList model constructor.
     * @property {module:model/ResponseJobList}
     */
    ResponseJobList: ResponseJobList,
    /**
     * The ResponseLegacyCampaigns model constructor.
     * @property {module:model/ResponseLegacyCampaigns}
     */
    ResponseLegacyCampaigns: ResponseLegacyCampaigns,
    /**
     * The ResponseUserList model constructor.
     * @property {module:model/ResponseUserList}
     */
    ResponseUserList: ResponseUserList,
    /**
     * The ScoredIndicatorType model constructor.
     * @property {module:model/ScoredIndicatorType}
     */
    ScoredIndicatorType: ScoredIndicatorType,
    /**
     * The AccountsApi service constructor.
     * @property {module:api/AccountsApi}
     */
    AccountsApi: AccountsApi,
    /**
     * The AgentsApi service constructor.
     * @property {module:api/AgentsApi}
     */
    AgentsApi: AgentsApi,
    /**
     * The AggregationsApi service constructor.
     * @property {module:api/AggregationsApi}
     */
    AggregationsApi: AggregationsApi,
    /**
     * The AnnotationsApi service constructor.
     * @property {module:api/AnnotationsApi}
     */
    AnnotationsApi: AnnotationsApi,
    /**
     * The BookmarksApi service constructor.
     * @property {module:api/BookmarksApi}
     */
    BookmarksApi: BookmarksApi,
    /**
     * The CallsApi service constructor.
     * @property {module:api/CallsApi}
     */
    CallsApi: CallsApi,
    /**
     * The CampaignsApi service constructor.
     * @property {module:api/CampaignsApi}
     */
    CampaignsApi: CampaignsApi,
    /**
     * The CampaignslegacyApi service constructor.
     * @property {module:api/CampaignslegacyApi}
     */
    CampaignslegacyApi: CampaignslegacyApi,
    /**
     * The CompaniesApi service constructor.
     * @property {module:api/CompaniesApi}
     */
    CompaniesApi: CompaniesApi,
    /**
     * The ConfirmationApi service constructor.
     * @property {module:api/ConfirmationApi}
     */
    ConfirmationApi: ConfirmationApi,
    /**
     * The EmployeesApi service constructor.
     * @property {module:api/EmployeesApi}
     */
    EmployeesApi: EmployeesApi,
    /**
     * The JobsApi service constructor.
     * @property {module:api/JobsApi}
     */
    JobsApi: JobsApi,
    /**
     * The MeApi service constructor.
     * @property {module:api/MeApi}
     */
    MeApi: MeApi,
    /**
     * The ProcessesApi service constructor.
     * @property {module:api/ProcessesApi}
     */
    ProcessesApi: ProcessesApi,
    /**
     * The ProfileApi service constructor.
     * @property {module:api/ProfileApi}
     */
    ProfileApi: ProfileApi,
    /**
     * The TenantApi service constructor.
     * @property {module:api/TenantApi}
     */
    TenantApi: TenantApi,
    /**
     * The UsersApi service constructor.
     * @property {module:api/UsersApi}
     */
    UsersApi: UsersApi
  };

  return exports;
}));
