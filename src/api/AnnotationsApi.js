/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorModel', 'model/RequestCallAnnotationModel', 'model/SuccessModel'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorModel'), require('../model/RequestCallAnnotationModel'), require('../model/SuccessModel'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.AnnotationsApi = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.ErrorModel, root.BehavioralsignalsUapiClient.RequestCallAnnotationModel, root.BehavioralsignalsUapiClient.SuccessModel);
  }
}(this, function(ApiClient, ErrorModel, RequestCallAnnotationModel, SuccessModel) {
  'use strict';

  /**
   * Annotations service.
   * @module api/AnnotationsApi
   * @version 0.2.18
   */

  /**
   * Constructs a new AnnotationsApi. 
   * @alias module:api/AnnotationsApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Removes the annotation lock of a call
     * Removes the annotation lock of a call
     * @param {Number} id The call id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.removeCallLockWithHttpInfo = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling removeCallLock");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/calls/{id}/lock/', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Removes the annotation lock of a call
     * Removes the annotation lock of a call
     * @param {Number} id The call id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.removeCallLock = function(id) {
      return this.removeCallLockWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Annotate highlights for a call
     * Annotate highlights for a call
     * @param {Number} callId The call id
     * @param {Object} opts Optional parameters
     * @param {module:model/RequestCallAnnotationModel} opts.callAnnotation Annotations
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.updateCallAnnotationWithHttpInfo = function(callId, opts) {
      opts = opts || {};
      var postBody = opts['callAnnotation'];

      // verify the required parameter 'callId' is set
      if (callId === undefined || callId === null) {
        throw new Error("Missing the required parameter 'callId' when calling updateCallAnnotation");
      }


      var pathParams = {
        'call_id': callId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/calls/{call_id}/annotations/', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Annotate highlights for a call
     * Annotate highlights for a call
     * @param {Number} callId The call id
     * @param {Object} opts Optional parameters
     * @param {module:model/RequestCallAnnotationModel} opts.callAnnotation Annotations
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.updateCallAnnotation = function(callId, opts) {
      return this.updateCallAnnotationWithHttpInfo(callId, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Locks or updates the annotation lock of a call
     * Locks or updates the annotation lock of a call
     * @param {Number} id The call id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.updateCallLockWithHttpInfo = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling updateCallLock");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/calls/{id}/lock/', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Locks or updates the annotation lock of a call
     * Locks or updates the annotation lock of a call
     * @param {Number} id The call id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.updateCallLock = function(id) {
      return this.updateCallLockWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
