/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/ErrorModel', 'model/ResponseUserList', 'model/SuccessModel', 'model/UserCreation', 'model/UserDetails', 'model/UserNotificationDetails'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/ErrorModel'), require('../model/ResponseUserList'), require('../model/SuccessModel'), require('../model/UserCreation'), require('../model/UserDetails'), require('../model/UserNotificationDetails'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.UsersApi = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.ErrorModel, root.BehavioralsignalsUapiClient.ResponseUserList, root.BehavioralsignalsUapiClient.SuccessModel, root.BehavioralsignalsUapiClient.UserCreation, root.BehavioralsignalsUapiClient.UserDetails, root.BehavioralsignalsUapiClient.UserNotificationDetails);
  }
}(this, function(ApiClient, ErrorModel, ResponseUserList, SuccessModel, UserCreation, UserDetails, UserNotificationDetails) {
  'use strict';

  /**
   * Users service.
   * @module api/UsersApi
   * @version 0.2.18
   */

  /**
   * Constructs a new UsersApi. 
   * @alias module:api/UsersApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Returns details for user with id user_id depending on permissions. If has admin permissions returns details for any user else returns only his own details.
     * Returns user details.
     * @param {Number} userId The user id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/UserDetails} and HTTP response
     */
    this.getUserDetailsWithHttpInfo = function(userId) {
      var postBody = null;

      // verify the required parameter 'userId' is set
      if (userId === undefined || userId === null) {
        throw new Error("Missing the required parameter 'userId' when calling getUserDetails");
      }


      var pathParams = {
        'user_id': userId
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserDetails;

      return this.apiClient.callApi(
        '/users/{user_id}/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Returns details for user with id user_id depending on permissions. If has admin permissions returns details for any user else returns only his own details.
     * Returns user details.
     * @param {Number} userId The user id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/UserDetails}
     */
    this.getUserDetails = function(userId) {
      return this.getUserDetailsWithHttpInfo(userId)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Returns list of users details depending on permissions. if user returns his own details if admin returns list of details for all users.
     * Returns a list of users or user details.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page Page number (default to 1)
     * @param {Number} opts.limit The maximum size of the page entries (default to 10)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResponseUserList} and HTTP response
     */
    this.getUserListWithHttpInfo = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'page': opts['page'],
        'limit': opts['limit'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ResponseUserList;

      return this.apiClient.callApi(
        '/users/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Returns list of users details depending on permissions. if user returns his own details if admin returns list of details for all users.
     * Returns a list of users or user details.
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page Page number (default to 1)
     * @param {Number} opts.limit The maximum size of the page entries (default to 10)
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResponseUserList}
     */
    this.getUserList = function(opts) {
      return this.getUserListWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Registers a new user
     * @param {module:model/UserDetails} user The user to create
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/UserCreation} and HTTP response
     */
    this.registerUserWithHttpInfo = function(user) {
      var postBody = user;

      // verify the required parameter 'user' is set
      if (user === undefined || user === null) {
        throw new Error("Missing the required parameter 'user' when calling registerUser");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = UserCreation;

      return this.apiClient.callApi(
        '/users/register/', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Registers a new user
     * @param {module:model/UserDetails} user The user to create
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/UserCreation}
     */
    this.registerUser = function(user) {
      return this.registerUserWithHttpInfo(user)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Updates the notified property of the user
     * @param {module:model/UserNotificationDetails} notifyFlag The notification flag object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.updateUserNotifyWithHttpInfo = function(notifyFlag) {
      var postBody = notifyFlag;

      // verify the required parameter 'notifyFlag' is set
      if (notifyFlag === undefined || notifyFlag === null) {
        throw new Error("Missing the required parameter 'notifyFlag' when calling updateUserNotify");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/me/notify/', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Updates the notified property of the user
     * @param {module:model/UserNotificationDetails} notifyFlag The notification flag object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.updateUserNotify = function(notifyFlag) {
      return this.updateUserNotifyWithHttpInfo(notifyFlag)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
