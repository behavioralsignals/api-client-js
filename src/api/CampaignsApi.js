/**
 * UAPI1
 * Behavioral Signal Technologies' API enables third party applications or processes to process collected data in the cloud and provides emotional and demographic insights for consumption.
 *
 * OpenAPI spec version: 0.2.18
 * Contact: uapi1@behavioralsignals.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 2.4.5
 *
 * Do not edit the class manually.
 *
 */

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module.
    define(['ApiClient', 'model/CalculationModel', 'model/Campaign', 'model/CampaignRequest', 'model/CreationModel', 'model/ErrorModel', 'model/ResponseAggregatedCampaign', 'model/ResponseAggregatedCampaigns', 'model/ResponseCampaigns', 'model/SuccessModel'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    module.exports = factory(require('../ApiClient'), require('../model/CalculationModel'), require('../model/Campaign'), require('../model/CampaignRequest'), require('../model/CreationModel'), require('../model/ErrorModel'), require('../model/ResponseAggregatedCampaign'), require('../model/ResponseAggregatedCampaigns'), require('../model/ResponseCampaigns'), require('../model/SuccessModel'));
  } else {
    // Browser globals (root is window)
    if (!root.BehavioralsignalsUapiClient) {
      root.BehavioralsignalsUapiClient = {};
    }
    root.BehavioralsignalsUapiClient.CampaignsApi = factory(root.BehavioralsignalsUapiClient.ApiClient, root.BehavioralsignalsUapiClient.CalculationModel, root.BehavioralsignalsUapiClient.Campaign, root.BehavioralsignalsUapiClient.CampaignRequest, root.BehavioralsignalsUapiClient.CreationModel, root.BehavioralsignalsUapiClient.ErrorModel, root.BehavioralsignalsUapiClient.ResponseAggregatedCampaign, root.BehavioralsignalsUapiClient.ResponseAggregatedCampaigns, root.BehavioralsignalsUapiClient.ResponseCampaigns, root.BehavioralsignalsUapiClient.SuccessModel);
  }
}(this, function(ApiClient, CalculationModel, Campaign, CampaignRequest, CreationModel, ErrorModel, ResponseAggregatedCampaign, ResponseAggregatedCampaigns, ResponseCampaigns, SuccessModel) {
  'use strict';

  /**
   * Campaigns service.
   * @module api/CampaignsApi
   * @version 0.2.18
   */

  /**
   * Constructs a new CampaignsApi. 
   * @alias module:api/CampaignsApi
   * @class
   * @param {module:ApiClient} [apiClient] Optional API client implementation to use,
   * default to {@link module:ApiClient#instance} if unspecified.
   */
  var exports = function(apiClient) {
    this.apiClient = apiClient || ApiClient.instance;



    /**
     * Create a new campaign
     * Create a new campaign
     * @param {module:model/Campaign} campaignRequest A campaign request object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/CreationModel} and HTTP response
     */
    this.createCampaignWithHttpInfo = function(campaignRequest) {
      var postBody = campaignRequest;

      // verify the required parameter 'campaignRequest' is set
      if (campaignRequest === undefined || campaignRequest === null) {
        throw new Error("Missing the required parameter 'campaignRequest' when calling createCampaign");
      }


      var pathParams = {
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = CreationModel;

      return this.apiClient.callApi(
        '/campaigns/', 'POST',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Create a new campaign
     * Create a new campaign
     * @param {module:model/Campaign} campaignRequest A campaign request object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/CreationModel}
     */
    this.createCampaign = function(campaignRequest) {
      return this.createCampaignWithHttpInfo(campaignRequest)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Delete a campaign
     * Deletes a campaign
     * @param {Number} id The campaign id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.deleteCampaignWithHttpInfo = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling deleteCampaign");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/campaigns/{id}/', 'DELETE',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Delete a campaign
     * Deletes a campaign
     * @param {Number} id The campaign id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.deleteCampaign = function(id) {
      return this.deleteCampaignWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Returns aggregations of a campaign.
     * Returns aggregations for campaign identified by id .
     * @param {Number} id The campaign id
     * @param {Object} opts Optional parameters
     * @param {String} opts.agentId Filter calls based on agent_id and calculate per campaign aggregates
     * @param {Date} opts.startDate Aggregates campaign based on the start of a date range
     * @param {Date} opts.endDate Aggregates campaign based on the end of a date range
     * @param {module:model/CampaignRequest} opts.campaignRequest Campaign data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResponseAggregatedCampaign} and HTTP response
     */
    this.getAggregatedCampaignWithHttpInfo = function(id, opts) {
      opts = opts || {};
      var postBody = opts['campaignRequest'];

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling getAggregatedCampaign");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
        'agent_id': opts['agentId'],
        'start_date': opts['startDate'],
        'end_date': opts['endDate'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/csv'];
      var returnType = ResponseAggregatedCampaign;

      return this.apiClient.callApi(
        '/aggregator/campaigns/{id}/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Returns aggregations of a campaign.
     * Returns aggregations for campaign identified by id .
     * @param {Number} id The campaign id
     * @param {Object} opts Optional parameters
     * @param {String} opts.agentId Filter calls based on agent_id and calculate per campaign aggregates
     * @param {Date} opts.startDate Aggregates campaign based on the start of a date range
     * @param {Date} opts.endDate Aggregates campaign based on the end of a date range
     * @param {module:model/CampaignRequest} opts.campaignRequest Campaign data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResponseAggregatedCampaign}
     */
    this.getAggregatedCampaign = function(id, opts) {
      return this.getAggregatedCampaignWithHttpInfo(id, opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Returns list of campaigns aggregations.
     * Returns list of campaigns aggregations.
     * @param {Object} opts Optional parameters
     * @param {String} opts.agentId Filter calls based on agent_id and calculate per campaign aggregates
     * @param {module:model/String} opts.sortBy Sorting option (default to size)
     * @param {Number} opts.page Page number (default to 1)
     * @param {Number} opts.limit The maximum size of the page entries (default to 20)
     * @param {Date} opts.startDate Aggregates campaigns based on the start of a date range
     * @param {Date} opts.endDate Aggregates campaigns based on the end of a date range
     * @param {module:model/CampaignRequest} opts.campaignRequest Campaign request data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResponseAggregatedCampaigns} and HTTP response
     */
    this.getAggregatedCampaignsWithHttpInfo = function(opts) {
      opts = opts || {};
      var postBody = opts['campaignRequest'];


      var pathParams = {
      };
      var queryParams = {
        'agent_id': opts['agentId'],
        'sort_by': opts['sortBy'],
        'page': opts['page'],
        'limit': opts['limit'],
        'start_date': opts['startDate'],
        'end_date': opts['endDate'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json', 'text/csv'];
      var returnType = ResponseAggregatedCampaigns;

      return this.apiClient.callApi(
        '/aggregator/campaigns/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Returns list of campaigns aggregations.
     * Returns list of campaigns aggregations.
     * @param {Object} opts Optional parameters
     * @param {String} opts.agentId Filter calls based on agent_id and calculate per campaign aggregates
     * @param {module:model/String} opts.sortBy Sorting option (default to size)
     * @param {Number} opts.page Page number (default to 1)
     * @param {Number} opts.limit The maximum size of the page entries (default to 20)
     * @param {Date} opts.startDate Aggregates campaigns based on the start of a date range
     * @param {Date} opts.endDate Aggregates campaigns based on the end of a date range
     * @param {module:model/CampaignRequest} opts.campaignRequest Campaign request data
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResponseAggregatedCampaigns}
     */
    this.getAggregatedCampaigns = function(opts) {
      return this.getAggregatedCampaignsWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Get a campaign
     * Get a campaign
     * @param {Number} id The campaign id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/Campaign} and HTTP response
     */
    this.getCampaignWithHttpInfo = function(id) {
      var postBody = null;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling getCampaign");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = Campaign;

      return this.apiClient.callApi(
        '/campaigns/{id}/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Get a campaign
     * Get a campaign
     * @param {Number} id The campaign id
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/Campaign}
     */
    this.getCampaign = function(id) {
      return this.getCampaignWithHttpInfo(id)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Returns a list of campaigns without aggregations
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page Page number
     * @param {Number} opts.limit The maximum size of the page entries
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/ResponseCampaigns} and HTTP response
     */
    this.getCampaignsWithHttpInfo = function(opts) {
      opts = opts || {};
      var postBody = null;


      var pathParams = {
      };
      var queryParams = {
        'page': opts['page'],
        'limit': opts['limit'],
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = ResponseCampaigns;

      return this.apiClient.callApi(
        '/campaigns/', 'GET',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Returns a list of campaigns without aggregations
     * @param {Object} opts Optional parameters
     * @param {Number} opts.page Page number
     * @param {Number} opts.limit The maximum size of the page entries
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/ResponseCampaigns}
     */
    this.getCampaigns = function(opts) {
      return this.getCampaignsWithHttpInfo(opts)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }


    /**
     * Update a campaign
     * Update a campaign
     * @param {Number} id The campaign id
     * @param {module:model/Campaign} campaignRequest A campaign request object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with an object containing data of type {@link module:model/SuccessModel} and HTTP response
     */
    this.updateCampaignWithHttpInfo = function(id, campaignRequest) {
      var postBody = campaignRequest;

      // verify the required parameter 'id' is set
      if (id === undefined || id === null) {
        throw new Error("Missing the required parameter 'id' when calling updateCampaign");
      }

      // verify the required parameter 'campaignRequest' is set
      if (campaignRequest === undefined || campaignRequest === null) {
        throw new Error("Missing the required parameter 'campaignRequest' when calling updateCampaign");
      }


      var pathParams = {
        'id': id
      };
      var queryParams = {
      };
      var collectionQueryParams = {
      };
      var headerParams = {
      };
      var formParams = {
      };

      var authNames = ['OAuth2'];
      var contentTypes = ['application/json'];
      var accepts = ['application/json'];
      var returnType = SuccessModel;

      return this.apiClient.callApi(
        '/campaigns/{id}/', 'PUT',
        pathParams, queryParams, collectionQueryParams, headerParams, formParams, postBody,
        authNames, contentTypes, accepts, returnType
      );
    }

    /**
     * Update a campaign
     * Update a campaign
     * @param {Number} id The campaign id
     * @param {module:model/Campaign} campaignRequest A campaign request object
     * @return {Promise} a {@link https://www.promisejs.org/|Promise}, with data of type {@link module:model/SuccessModel}
     */
    this.updateCampaign = function(id, campaignRequest) {
      return this.updateCampaignWithHttpInfo(id, campaignRequest)
        .then(function(response_and_data) {
          return response_and_data.data;
        });
    }
  };

  return exports;
}));
